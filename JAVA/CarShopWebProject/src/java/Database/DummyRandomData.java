package Database;

import java.io.IOException;
import java.util.Random;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.*;
import javax.servlet.http.*;
/**
 *
 * @author klric
 */
public class DummyRandomData extends HttpServlet
{
    private static Random rand = new Random();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }
    
    public static int Generator(String dataRec) {
        int min = Integer.parseInt(dataRec.split("-")[0]);
        int max = Integer.parseInt(dataRec.split("-")[1]);
        return rand.nextInt((max - min) + 1) + min;
    }
    public static String createJson(String release, String engine, String hp, String price) 
    {
        JsonObjectBuilder model = Json.createObjectBuilder();
        model.add("RELEASE", Generator(release));
        model.add("ENGINE", Generator(engine));
        model.add("HP", Generator(hp));
        model.add("PRICE", Generator(price));
        JsonObject  done = model.build();
        return done.toString();
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        processRequest(request, response);
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
