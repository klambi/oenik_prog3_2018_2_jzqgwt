<%-- 
    Document   : index
    Created on : 2018.11.23., 17:12:33
    Author     : klric
--%>

<%@page import="Database.DummyRandomData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JZQGWT's Dummy Data Generator for CarShop</title>
    <body>
        <%if (request.getParameter("release") == null || request.getParameter("engine") == null || request.getParameter("hp") == null || request.getParameter("price") == null) {%>
            <%out.println("Paraméter hiba!");%>
        <%} else {%>
                <% try {
                    String release = request.getParameter("release");
                    if (!release.contains("-")) {
                        out.println("Formátum hiba! Megfelelő formátum:X-Y");
                        return;
                    }
                    String engine = request.getParameter("engine");
                    if (!engine.contains("-")) {
                        out.println("Formátum hiba! Megfelelő formátum: X-Y");
                        return;
                    }
                    String hp = request.getParameter("hp");
                    if (!hp.contains("-")) {
                        out.println("Formátum hiba! Megfelelő formátum: X-Y");
                        return;
                    }
                    String price = request.getParameter("price");
                    if (!price.contains("-")) {
                        out.println("Formátum hiba! Megfelelő formátum: X-Y");
                        return;
                    }
                    out.println(DummyRandomData.createJson(release, engine, hp, price));
                }
                catch (Exception e) {
                    out.println("Hiba! " + e);
                }%>
        <%}%>
    </body>
    </head>
</html>
