﻿//-----------------------------------------------------------------------
// <copyright file="ModellsRepo.cs" company="Klambauer Richárd">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// This class represents models table.
    /// </summary>
    public class ModellsRepo : IRepository<modells>
    {
        /// <summary>
        /// Local data.
        /// </summary>
        private List<modells> modells;

        /// <summary>
        /// Creates a data.
        /// </summary>
        /// <returns>Return parameter.</returns>
        public virtual IQueryable<modells> GetAll()
        {
            return Handler.GetAllModells().AsQueryable();
        }

        /// <summary>
        /// Creates record in table.
        /// </summary>
        /// <param name="brandid">With this.</param>
        /// <param name="name">And with this.</param>
        /// <param name="releasedate">Also with this.</param>
        /// <param name="enginevolume">Furthermore with this.</param>
        /// <param name="horsepower">Don't forget this.</param>
        /// <param name="baseprice">Nor this.</param>
        /// <returns>True on success.</returns>
        public virtual bool Create(int brandid, string name, DateTime releasedate, int enginevolume, int horsepower, int baseprice)
        {
            modells modell = new modells();
            modell.brand_id = brandid;
            modell.name = name;
            modell.date = releasedate;
            modell.engineinfo = enginevolume;
            modell.hp = horsepower;
            modell.price = baseprice;
            Handler.AddNewModell(modell);
            Handler.SaveDB();
            return true;
        }

        /// <summary>
        /// Removes record.
        /// </summary>
        /// <param name="id">Where ID is this.</param>
        /// <returns>True on success.</returns>
        public virtual bool Remove(int id)
        {
            if (this.CheckIfModellIdExist(id))
            {
                List<modells> m = Handler.GetAllModells().Where(x => x.Id.Equals(id)).Select(o => o).ToList();
                Handler.RemoveModell(m[0]);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Updates table.
        /// </summary>
        /// <param name="id">With this.</param>
        /// <param name="brandid">And this.</param>
        /// <param name="name">And with this.</param>
        /// <param name="releasedate">Also with this.</param>
        /// <param name="enginevolume">Even this.</param>
        /// <param name="horsepower">Further more this.</param>
        /// <param name="baseprice">And last this.</param>
        /// <returns>True on success.</returns>
        public virtual bool Update(int id, int brandid, string name, DateTime releasedate, int enginevolume, int horsepower, int baseprice)
        {
            List<modells> query = Handler.GetAllModells().Where(x => x.Id == id).Select(x2 => x2).ToList();
            if (query.Count > 0)
            {
                modells modell = query[0];
                modell.Id = Handler.GetLastId(modell);
                modell.brand_id = brandid;
                modell.name = name;
                modell.date = releasedate;
                modell.engineinfo = enginevolume;
                modell.hp = horsepower;
                modell.price = baseprice;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the data of the table.
        /// </summary>
        /// <returns>Returns a string with the data.</returns>
        public virtual StringBuilder GetTableContents()
        {
            List<modells> temp = this.GetAll().ToList();
            StringBuilder builder = new StringBuilder();
            foreach (var x in temp)
            {
                builder.AppendLine("-* ID: " + x.Id + " | NÉV: " + x.name + " | MÁRKA_ID: " + x.brand_id + " | LÓERŐ: " + x.hp + " | ÁR: " + x.price + " | MOTOR_TÉRFOGAT: " + x.engineinfo + " |  MEGJELENÉS: " + x.date);
            }

            return builder;
        }

        /// <summary>
        /// Checks for specific ID.
        /// </summary>
        /// <param name="id">ID to look for.</param>
        /// <returns>True if exists.</returns>
        public virtual bool CheckIfModellIdExist(int id)
        {
            return this.GetAll().Any(x => x.Id == id);
        }

        /// <summary>
        /// Gets data.
        /// </summary>
        /// <param name="brandId">Of this id.</param>
        /// <returns>Returns average.</returns>
        public virtual double GetAverageAmountForBrand(int brandId)
        {
            double brandcount = this.GetAll().Where(x => x.brand_id == brandId).Select(x2 => x2).ToList().Count;
            double all = this.GetAll().Select(x2 => x2).ToList().Count;
            return brandcount / all;
        }

        /// <summary>
        /// Loads the database to a local variable.
        /// </summary>
        public virtual void LoadModells()
        {
            List<modells> kocsik = Handler.GetAllModells();
            this.modells = kocsik;
        }
    }
}
