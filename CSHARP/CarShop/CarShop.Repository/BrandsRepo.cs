﻿//-----------------------------------------------------------------------
// <copyright file="BrandsRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// This class represents the brands table.
    /// </summary>
    public class BrandsRepo : IRepository<brands>
    {
        /// <summary>
        /// Instance of a list we use.
        /// </summary>
        private List<brands> brands;

        /// <summary>
        /// Asks for all brands.
        /// </summary>
        /// <returns>Returns all brands.</returns>
        public virtual IQueryable<brands> GetAll()
        {
            return this.brands.AsQueryable<brands>();
        }

        /// <summary>
        /// Inserts record.
        /// </summary>
        /// <param name="name">Brand name.</param>
        /// <param name="country">Base country.</param>
        /// <param name="url">Website of brand.</param>
        /// <param name="foundation">Year of foundation.</param>
        /// <param name="annualtraffic">Annual income.</param>
        /// <returns>True on success.</returns>
        public virtual bool Create(string name, string country, string url, int foundation, int annualtraffic)
        {
            brands brand = new brands();
            brand.Id = Handler.GetLastId(brand);
            brand.name = name;
            brand.country = country;
            brand.url = url;
            brand.foundation = foundation;
            brand.income = annualtraffic;
            Handler.AddNewBrand(brand);
            Handler.SaveDB();
            return true;
        }

        /// <summary>
        /// Deletes record.
        /// </summary>
        /// <param name="id">ID of record.</param>
        /// <returns>True on success.</returns>
        public virtual bool Remove(int id)
        {
            if (this.CheckIfBrandExists(id))
            {
                List<brands> b = Handler.GetAllBrands().Where(x => x.Id.Equals(id)).Select(x => x).ToList();
                Handler.RemoveBrand(b[0]);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Updates data.
        /// </summary>
        /// <param name="id">ID of record.</param>
        /// <param name="name">Brand name.</param>
        /// <param name="country">Base country of brand.</param>
        /// <param name="url">Website of brand.</param>
        /// <param name="foundation">Year of foundation.</param>
        /// <param name="annualtraffic">Annual income of company.</param>
        /// <returns>True on success.</returns>
        public virtual bool Update(int id, string name, string country, string url, int foundation, int annualtraffic)
        {
            List<brands> query = Handler.GetAllBrands().Where(x => x.Id == id).Select(x2 => x2).ToList();
            if (query.Count > 0)
            {
                brands marka = query[0];
                marka.name = name;
                marka.country = country;
                marka.url = url;
                marka.foundation = foundation;
                marka.income = annualtraffic;

                return true;
            }

            return false;
        }

        /// <summary>
        /// Builds a specific string.
        /// </summary>
        /// <returns>Returns with a string.</returns>
        public virtual StringBuilder GetTableContents()
        {
            List<brands> temp = this.GetAll().ToList();
            StringBuilder builder = new StringBuilder();
            foreach (var x in temp)
            {
                builder.AppendLine("-* ID: " + x.Id + " | NÉV: " + x.name + " | ORSZÁG: " + x.country + " | URL: " + x.url + " | ÉVES BEVÉTEL: " + x.income + " | ALAPÍTÁS ÉVE: " + x.foundation);
            }

            return builder;
        }

        /// <summary>
        /// Checks if brand exists.
        /// </summary>
        /// <param name="id">ID to look for.</param>
        /// <returns>True if exists.</returns>
        public virtual bool CheckIfBrandExists(int id)
        {
            return this.GetAll().Any(x => x.Id == id);
        }

        /// <summary>
        /// Loads the brands.
        /// </summary>
        public virtual void LoadBrands()
        {
            List<brands> brands = Handler.GetAllBrands();
            this.brands = brands;
        }

        /// <summary>
        /// Gets data.
        /// </summary>
        /// <param name="countryname">Base country.</param>
        /// <returns>Returns average.</returns>
        public virtual double GetAverageForBrandAmountByCountry(string countryname)
        {
            countryname = countryname.ToLower();
            double brandcount = this.GetAll().Where(x => x.country.ToLower() == countryname).Select(x2 => x2).ToList().Count;
            double all = this.GetAll().Select(x2 => x2).ToList().Count;
            return brandcount / all;
        }
    }
}
