﻿//-----------------------------------------------------------------------
// <copyright file="Helper.cs" company="Klambauer Richárd">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// This class is helping the repository to be able to work.
    /// </summary>
    public class Helper
    {
        /// <summary>
        /// Repository of brands.
        /// </summary>
        private readonly BrandsRepo brandrepo;

        /// <summary>
        /// Repository of models.
        /// </summary>
        private readonly ModellsRepo modellrepo;

        /// <summary>
        /// Repository of extras.
        /// </summary>
        private readonly ExtrasRepo extrasrepo;

        /// <summary>
        /// Repository of model's extras.
        /// </summary>
        private readonly ModellsExtrasRepo modellsextrasrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="Helper"/> class.
        /// </summary>
        public Helper()
        {
            this.brandrepo = new BrandsRepo();
            this.brandrepo.LoadBrands();
            this.modellrepo = new ModellsRepo();
            this.modellrepo.LoadModells();
            this.extrasrepo = new ExtrasRepo();
            this.extrasrepo.LoadExtras();
            this.modellsextrasrepo = new ModellsExtrasRepo();
            this.modellsextrasrepo.LoadModellsExtras();
        }

        /// <summary>
        /// Gets data.
        /// </summary>
        public virtual BrandsRepo BrandsRepo
        {
            get
            {
                return this.brandrepo;
            }
        }

        /// <summary>
        /// Gets data.
        /// </summary>
        public virtual ModellsRepo ModellsRepo
        {
            get
            {
                return this.modellrepo;
            }
        }

        /// <summary>
        /// Gets data.
        /// </summary>
        public virtual ExtrasRepo ExtrasRepo
        {
            get
            {
                return this.extrasrepo;
            }
        }

        /// <summary>
        /// Gets data.
        /// </summary>
        public virtual ModellsExtrasRepo ModellsExtrasRepo
        {
            get { return this.modellsextrasrepo; }
        }

        /// <summary>
        /// Receive table names.
        /// </summary>
        /// <returns>Return Parameter.</returns>
        public virtual List<string> GetAllTableNames()
        {
            return Handler.TablesNames;
        }
    }
}
