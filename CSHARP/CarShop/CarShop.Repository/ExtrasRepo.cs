﻿//-----------------------------------------------------------------------
// <copyright file="ExtrasRepo.cs" company="Klambauer Richárd">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// This class represents one of the extras database, like one record of the table.
    /// </summary>
    public class ExtrasRepo : IRepository<extras>
    {
        /// <summary>
        /// List of tables content.
        /// </summary>
        private List<extras> extras;

        /// <summary>
        /// Gets the data from the full database.
        /// </summary>
        /// <returns>Returns objects.</returns>
        public virtual IQueryable<extras> GetAll()
        {
            return Handler.GetAllExtras().AsQueryable();
        }

        /// <summary>
        /// Inserts data.
        /// </summary>
        /// <param name="category">With 1.</param>
        /// <param name="name">With 2.</param>
        /// <param name="price">With 3.</param>
        /// <param name="color">With 4.</param>
        /// <param name="reusable">With 5.</param>
        /// <returns>True on success.</returns>
        public virtual bool Create(string category, string name, int price, string color, bool reusable)
        {
            byte i = 0;
            if (reusable)
            {
                i = 1;
            }

            extras extra = new extras();
            extra.Id = Handler.GetLastId(extra);
            extra.category = category;
            extra.name = name;
            extra.price = price;
            extra.color = color;
            extra.reusable = i;
            Handler.AddNewExtra(extra);
            Handler.SaveDB();
            return true;
        }

        /// <summary>
        /// Deletes record.
        /// </summary>
        /// <param name="id">ID of the record.</param>
        /// <returns>True on success.</returns>
        public virtual bool Remove(int id)
        {
            if (this.CheckIfExtraIdExist(id))
            {
                List<extras> e = Handler.GetAllExtras().Where(x => x.Id.Equals(id)).Select(o => o).ToList();
                Handler.RemoveExtra(e[0]);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Updates database.
        /// </summary>
        /// <param name="id">Id of object.</param>
        /// <param name="category">Category name of the object.</param>
        /// <param name="name">Name of the object.</param>
        /// <param name="price">Price of the object.</param>
        /// <param name="color">Color of the object.</param>
        /// <param name="reusable">Is the object reusable?.</param>
        /// <returns>True on success.</returns>
        public virtual bool Update(int id, string category, string name, int price, string color, bool reusable)
        {
            List<extras> extras = Handler.GetAllExtras().Where(x => x.Id == id).Select(x2 => x2).ToList();
            if (extras.Count > 0)
            {
                byte i = 0;
                if (reusable)
                {
                    i = 1;
                }

                extras extra = extras[0];
                extra.name = name;
                extra.category = category;
                extra.price = price;
                extra.color = color;
                extra.reusable = i;

                return true;
            }

            return false;
        }

        /// <summary>
        /// Builds a string with the content of the table.
        /// </summary>
        /// <returns>Returns custom string.</returns>
        public virtual StringBuilder GetTableContents()
        {
            List<extras> temp = this.GetAll().ToList();
            StringBuilder builder = new StringBuilder();
            foreach (var x in temp)
            {
                builder.AppendLine("-* ID: " + x.Id + " | NÉV: " + x.name + " | KATEGÓRIA: " + x.category + " | SZÍN: " + x.color + " | ÁR: " + x.price + " | TÖBBSZÖR HASZNÁLHATÓ: " + x.reusable);
            }

            return builder;
        }

        /// <summary>
        /// Gets data.
        /// </summary>
        /// <param name="color">Color parameter of the extra.</param>
        /// <returns>Returns average.</returns>
        public virtual double GetAverageForExtraColor(string color)
        {
            double brandcount = this.GetAll().Where(x => x.color.ToLower() == color.ToLower()).Select(x2 => x2).ToList().Count;
            double all = this.GetAll().Select(x2 => x2).ToList().Count;
            return brandcount / all;
        }

        /// <summary>
        /// Checks if there is an extra with the specified ID.
        /// </summary>
        /// <param name="id">ID we are looking for.</param>
        /// <returns>True if exists.</returns>
        public virtual bool CheckIfExtraIdExist(int id)
        {
            return this.GetAll().Any(x => x.Id == id);
        }

        /// <summary>
        /// Loads the extras from the database to a local variable.
        /// </summary>
        public virtual void LoadExtras()
        {
            List<extras> extras = Handler.GetAllExtras();
            this.extras = extras;
        }
    }
}
