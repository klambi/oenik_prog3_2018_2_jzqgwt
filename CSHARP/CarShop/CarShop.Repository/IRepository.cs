﻿//-----------------------------------------------------------------------
// <copyright file="IRepository.cs" company="Klambauer Richárd">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface of repository commands.
    /// </summary>
    /// <typeparam name="T">T Parameter.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Gets string of table.
        /// </summary>
        /// <returns>Returns string.</returns>
        StringBuilder GetTableContents();

        /// <summary>
        /// Gets data.
        /// </summary>
        /// <returns>Returns data.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Deletes record.
        /// </summary>
        /// <param name="id">with this id.</param>
        /// <returns>True on success.</returns>
        bool Remove(int id);
    }
}
