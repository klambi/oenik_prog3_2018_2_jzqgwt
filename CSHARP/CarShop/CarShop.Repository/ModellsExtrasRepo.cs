﻿//-----------------------------------------------------------------------
// <copyright file="ModellsExtrasRepo.cs" company="Klambauer Richárd">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using CarShop.Data;

    /// <summary>
    /// This class represents the modell's extra's table combo.
    /// </summary>
    public class ModellsExtrasRepo : IRepository<modellsextras>
    {
        /// <summary>
        /// Local instance of model's extras.
        /// </summary>
        private List<modellsextras> modellsextras;

        /// <summary>
        /// Gets data.
        /// </summary>
        /// <returns>Returns data of table.</returns>
        public virtual IQueryable<modellsextras> GetAll()
        {
            return Handler.GetAllModellsExtras().AsQueryable();
        }

        /// <summary>
        /// Inserts to table.
        /// </summary>
        /// <param name="modellid">For this modell.</param>
        /// <param name="extraid">With this extra.</param>
        /// <returns>True on success.</returns>
        public virtual bool Create(int modellid, int extraid)
        {
            List<modellsextras> me = Handler.GetAllModellsExtras().Where(x => x.Id == modellid).Select(x2 => x2).ToList();
            foreach (var modellextra in me)
            {
                if (modellextra.extraid == extraid)
                {
                    List<extras> extras = Handler.GetAllExtras().Where(x => x.Id == extraid && x.reusable == 0).Select(x2 => x2).ToList();
                    if (extras.Count > 0)
                    {
                        return false;
                    }
                }
            }

            modellsextras data = new modellsextras();
            data.Id = Handler.GetLastId(data);
            data.modellid = modellid;
            data.extraid = extraid;
            Handler.AddNewModellExtra(data);
            Handler.SaveDB();
            return true;
        }

        /// <summary>
        /// Deletes record.
        /// </summary>
        /// <param name="id">With this id.</param>
        /// <returns>True on success.</returns>
        public virtual bool Remove(int id)
        {
            if (this.CheckIfModellsExtrasIdExists(id))
            {
                List<modellsextras> me = Handler.GetAllModellsExtras().Where(x => x.Id.Equals(id)).Select(o => o).ToList();
                Handler.RemoveModellExtra(me[0]);
                return true;
            }

            return false;
        }

        /// <summary>
        /// String builder.
        /// </summary>
        /// <returns>Returns custom string.</returns>
        public virtual StringBuilder GetTableContents()
        {
            List<modellsextras> temp = this.GetAll().ToList();
            StringBuilder sb = new StringBuilder();
            foreach (var x in temp)
            {
                sb.AppendLine("-* ID: " + x.Id + " | MODELLID: " + x.modellid + " | EXTRAID: " + x.extraid);
            }

            return sb;
        }

        /// <summary>
        /// Checks if exists.
        /// </summary>
        /// <param name="id">Of this id.</param>
        /// <returns>True if exists.</returns>
        public virtual bool DoesModellExist(int id)
        {
            List<modells> modells = Handler.GetAllModells().Where(x => x.Id == id).Select(x2 => x2).ToList();
            return modells.Count > 0;
        }

        /// <summary>
        /// Checks if exists.
        /// </summary>
        /// <param name="id">Of this id.</param>
        /// <returns>True if exists.</returns>
        public virtual bool DoesExtraExist(int id)
        {
            List<extras> extras = Handler.GetAllExtras().Where(x => x.Id == id).Select(x2 => x2).ToList();
            return extras.Count > 0;
        }

        /// <summary>
        /// Loads table from database to local variable.
        /// </summary>
        public virtual void LoadModellsExtras()
        {
            List<modellsextras> modellsextras = Handler.GetAllModellsExtras();
            this.modellsextras = modellsextras;
        }

        /// <summary>
        /// Check is record exists.
        /// </summary>
        /// <param name="id">Id we check.</param>
        /// <returns>True if exists.</returns>
        public virtual bool CheckIfModellsExtrasIdExists(int id)
        {
            return this.GetAll().Any(x => x.Id == id);
        }
    }
}