﻿//-----------------------------------------------------------------------
// <copyright file="Logic.cs" company="Klambauer Richárd">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Repository;

    /// <summary>
    /// Implements ILogic interface into business logic.
    /// </summary>
    public class Logic : ILogic
    {
        /// <summary>
        /// Creating an instance of RepositoryHelper for use.
        /// </summary>
        private readonly Helper helper;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        public Logic()
        {
            this.helper = new Helper();
        }

        /// <summary>
        /// Gets helper.
        /// </summary>
        public virtual Helper Helper
        {
            get
            {
                return this.helper;
            }
        }

        /// <summary>
        /// Return with all the table names.
        /// </summary>
        /// <returns>String built with the names of the table.</returns>
        public virtual List<string> GetAllTableNames()
        {
            return this.helper.GetAllTableNames();
        }

        /// <summary>
        /// Updates data in table.
        /// </summary>
        /// <param name="id">Id of record.</param>
        /// <param name="brandid">Brand's id of model.</param>
        /// <param name="name">Name of model.</param>
        /// <param name="releasedate">Release date of model.</param>
        /// <param name="engine">Info of engine.</param>
        /// <param name="hp">Horse power of engine.</param>
        /// <param name="price">Base price of model.</param>
        /// <returns>True on success.</returns>
        public virtual bool UpdateModellData(int id, int brandid, string name, DateTime releasedate, int engine, int hp, int price)
        {
            try
            {
                return this.helper.ModellsRepo.Update(id, brandid, name, releasedate, engine, hp, price);
            }
            catch (Exception e)
            {
                Console.WriteLine("[Frissítési hiba] " + id + " ID frissítése során hibába ütköztünk: " + e);
            }

            return false;
        }

        /// <summary>
        /// Updates data in table.
        /// </summary>
        /// <param name="id">ID of record.</param>
        /// <param name="name">Name of item.</param>
        /// <param name="country">Country name.</param>
        /// <param name="url">URL line.</param>
        /// <param name="foundation">Year of foundation.</param>
        /// <param name="income">Annual traffic.</param>
        /// <returns>True on success.</returns>
        public virtual bool UpdateBrandData(int id, string name, string country, string url, int foundation, int income)
        {
            try
            {
                return this.helper.BrandsRepo.Update(id, name, country, url, foundation, income);
            }
            catch (Exception e)
            {
                Console.WriteLine("[Frissítési hiba] " + id + " ID frissítése során hibába ütköztünk: " + e);
            }

            return false;
        }

        /// <summary>
        /// Updates data in table.
        /// </summary>
        /// <param name="id">ID of record.</param>
        /// <param name="category">Name of category.</param>
        /// <param name="name">Name of item.</param>
        /// <param name="price">Price of item.</param>
        /// <param name="color">Color of item.</param>
        /// <param name="reusable">Is it reusable.</param>
        /// <returns>True on success.</returns>
        public virtual bool UpdateExtraData(int id, string category, string name, int price, string color, bool reusable)
        {
            try
            {
                return this.helper.ExtrasRepo.Update(id, category, name, price, color, reusable);
            }
            catch (Exception e)
            {
                Console.WriteLine("[Frissítési hiba] " + id + " ID frissítése során hibába ütköztünk: " + e);
            }

            return false;
        }

        /// <summary>
        /// Inserts data into table.
        /// </summary>
        /// <param name="brandid">Brand's id of model.</param>
        /// <param name="name">Name of model.</param>
        /// <param name="releasedate">Release date of model.</param>
        /// <param name="engine">Info of engine.</param>
        /// <param name="hp">Horse power of engine.</param>
        /// <param name="price">Base price of model.</param>
        /// <returns>True on success.</returns>
        public virtual bool CreateModellData(int brandid, string name, DateTime releasedate, int engine, int hp, int price)
        {
            try
            {
                return this.helper.ModellsRepo.Create(brandid, name, releasedate, engine, hp, price);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Beillesztési hiba] " + name + " név tag-ű adat beillesztése során hibába ütköztünk: " + ex);
            }

            return false;
        }

        /// <summary>
        /// Inserts data into table.
        /// </summary>
        /// <param name="name">Brand name.</param>
        /// <param name="country">Country name.</param>
        /// <param name="url">URL line.</param>
        /// <param name="foundation">Year of foundation.</param>
        /// <param name="income">Annual traffic.</param>
        /// <returns>True on success.</returns>
        public virtual bool CreateBrandData(string name, string country, string url, int foundation, int income)
        {
            try
            {
                return this.helper.BrandsRepo.Create(name, country, url, foundation, income);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Beillesztési hiba] " + name + " név tag-ű adat beillesztése során hibába ütköztünk: " + ex);
            }

            return false;
        }

        /// <summary>
        /// Inserts data to table.
        /// </summary>
        /// <param name="category">Name of category.</param>
        /// <param name="name">Name of item.</param>
        /// <param name="price">Price of item.</param>
        /// <param name="color">Color of item.</param>
        /// <param name="reusable">Is it reusable.</param>
        /// <returns>True on success.</returns>
        public virtual bool CreateExtraData(string category, string name, int price, string color, bool reusable)
        {
            try
            {
                return this.helper.ExtrasRepo.Create(category, name, price, color, reusable);
            }
            catch (Exception e)
            {
                Console.WriteLine("[Beillesztési hiba] " + name + " név tag-ű adat beillesztése során hibába ütköztünk: " + e);
            }

            return false;
        }

        /// <summary>
        /// Creates data.
        /// </summary>
        /// <param name="modellid">Of this modell.</param>
        /// <param name="extraid">Of this extra.</param>
        /// <returns>True on success.</returns>
        public virtual bool CreateModellsExtraData(int modellid, int extraid)
        {
            try
            {
                return this.helper.ModellsExtrasRepo.Create(modellid, extraid);
            }
            catch (Exception e)
            {
                Console.WriteLine("[Beillesztési hiba] " + modellid + " modell_ID-jű adat beillesztése során hibába ütköztünk: " + e);
                throw;
            }
        }

        /// <summary>
        /// Deletes record of specific Id.
        /// </summary>
        /// <param name="id">ID of record.</param>
        /// <returns>True on success.</returns>
        public virtual bool RemoveModellData(int id)
        {
            try
            {
                return this.helper.ModellsRepo.Remove(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Törlési hiba] " + id + " ID törlése során hibába ütköztünk: " + ex);
            }

            return false;
        }

        /// <summary>
        /// Deletes record of specific Id.
        /// </summary>
        /// <param name="id">ID of record.</param>
        /// <returns>True on success.</returns>
        public virtual bool RemoveExtraData(int id)
        {
            try
            {
                return this.helper.ExtrasRepo.Remove(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Törlési hiba] " + id + " ID törlése során hibába ütköztünk: " + ex);
            }

            return false;
        }

        /// <summary>
        /// Deletes record of specific Id.
        /// </summary>
        /// <param name="id">ID of record.</param>
        /// <returns>True on success.</returns>
        public virtual bool RemoveBrandData(int id)
        {
            try
            {
                return this.helper.BrandsRepo.Remove(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Törlési hiba] " + id + " ID törlése során hibába ütköztünk: " + ex);
            }

            return false;
        }

        /// <summary>
        /// Deletes record.
        /// </summary>
        /// <param name="id">With this.</param>
        /// <returns>True on success.</returns>
        public virtual bool RemoveModellsExtraData(int id)
        {
            try
            {
                return this.helper.ModellsExtrasRepo.Remove(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("[Törlési hiba] " + id + " ID törlése során hibába ütköztünk: " + e);
            }

            return false;
        }

        /// <summary>
        /// Checks if exists.
        /// </summary>
        /// <param name="id">With this.</param>
        /// <returns>True if exists.</returns>
        public virtual bool DoesModellExist(int id)
        {
            return this.helper.ModellsExtrasRepo.DoesModellExist(id);
        }

        /// <summary>
        /// Checks if exists.
        /// </summary>
        /// <param name="id">With this.</param>
        /// <returns>True if exists.</returns>
        public virtual bool DoesExtraExist(int id)
        {
            return this.helper.ModellsExtrasRepo.DoesExtraExist(id);
        }

        /// <summary>
        /// Checks if the specific record exists.
        /// </summary>
        /// <param name="id">ID of brand.</param>
        /// <returns>True if exists.</returns>
        public virtual bool CheckIfBrandExists(int id)
        {
            return this.helper.BrandsRepo.CheckIfBrandExists(id);
        }

        /// <summary>
        /// Checks if the specific record exists.
        /// </summary>
        /// <param name="id">ID of brand.</param>
        /// <returns>True if exists.</returns>
        public virtual bool CheckIfModellExists(int id)
        {
            return this.helper.ModellsRepo.CheckIfModellIdExist(id);
        }

        /// <summary>
        /// Checks if the specific record exists.
        /// </summary>
        /// <param name="id">ID of brand.</param>
        /// <returns>True if exists.</returns>
        public virtual bool CheckIfExtraExists(int id)
        {
            return this.helper.ExtrasRepo.CheckIfExtraIdExist(id);
        }

        /// <summary>
        /// Creates a custom string from the tables content.
        /// </summary>
        /// <returns>Returns a string.</returns>
        public virtual StringBuilder GetBrandsData()
        {
            return this.helper.BrandsRepo.GetTableContents();
        }

        /// <summary>
        /// Creates a custom string from the tables content.
        /// </summary>
        /// <returns>Returns a string.</returns>
        public virtual StringBuilder GetExtrasData()
        {
            return this.helper.ExtrasRepo.GetTableContents();
        }

        /// <summary>
        /// Creates a custom string from the tables content.
        /// </summary>
        /// <returns>Returns a string.</returns>
        public virtual StringBuilder GetModellsData()
        {
            return this.helper.ModellsRepo.GetTableContents();
        }

        /// <summary>
        /// Gets model's extras from database.
        /// </summary>
        /// <returns>String of content.</returns>
        public virtual StringBuilder GetModelExtrasData()
        {
            return this.helper.ModellsExtrasRepo.GetTableContents();
        }

        /// <summary>
        /// Gets average amount of brand.
        /// </summary>
        /// <param name="brandid">ID of brand.</param>
        /// <returns>Returns average.</returns>
        public virtual double GetAverageAmountForBrand(int brandid)
        {
            try
            {
                return this.helper.ModellsRepo.GetAverageAmountForBrand(brandid);
            }
            catch (Exception e)
            {
                Console.WriteLine("[Átlag számítási hiba] " + brandid + " " + e);
            }

            return -1;
        }

        /// <summary>
        /// Gets average of extras color.
        /// </summary>
        /// <param name="color">Color we look for.</param>
        /// <returns>Returns average.</returns>
        public virtual double GetAverageForExtraColor(string color)
        {
            try
            {
                return this.helper.ExtrasRepo.GetAverageForExtraColor(color);
            }
            catch (Exception e)
            {
                Console.WriteLine("[Átlag számítási hiba] " + color + " " + e);
            }

            return -1;
        }

        /// <summary>
        /// Gets average amount of brands by country.
        /// </summary>
        /// <param name="country">Country name.</param>
        /// <returns>Number of brands in country.</returns>
        public virtual double GetAverageAmountForBrandByCountry(string country)
        {
            try
            {
                return this.helper.BrandsRepo.GetAverageForBrandAmountByCountry(country);
            }
            catch (Exception e)
            {
                Console.WriteLine("[Átlag számítási hiba] " + country + " " + e);
            }

            return -1;
        }
    }
}
