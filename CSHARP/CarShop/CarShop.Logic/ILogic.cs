﻿//-----------------------------------------------------------------------
// <copyright file="ILogic.cs" company="Klambauer Richárd">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Repository;

    /// <summary>
    /// Interface of business logic.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Gets table names.
        /// </summary>
        /// <returns>Names of tables.</returns>
        List<string> GetAllTableNames();

        /// <summary>
        /// Creates data.
        /// </summary>
        /// <param name="brandid">For brand.</param>
        /// <param name="name">With name.</param>
        /// <param name="releasedate">With release.</param>
        /// <param name="engine">With engine.</param>
        /// <param name="hp">With horse power.</param>
        /// <param name="price">With price.</param>
        /// <returns>True on success.</returns>
        bool CreateModellData(int brandid, string name, DateTime releasedate, int engine, int hp, int price);

        /// <summary>
        /// Creates data.
        /// </summary>
        /// <param name="name">With name.</param>
        /// <param name="country">With country name.</param>
        /// <param name="url">With URL.</param>
        /// <param name="foundation">With year of foundation.</param>
        /// <param name="income">With annual income.</param>
        /// <returns>True on success.</returns>
        bool CreateBrandData(string name, string country, string url, int foundation, int income);

        /// <summary>
        /// Creates data.
        /// </summary>
        /// <param name="category">Name of category.</param>
        /// <param name="name">Name of item.</param>
        /// <param name="price">Price of item.</param>
        /// <param name="color">Color of item.</param>
        /// <param name="reuseable">Is it usable again?.</param>
        /// <returns>True on success.</returns>
        bool CreateExtraData(string category, string name, int price, string color, bool reuseable);

        /// <summary>
        /// Creates data.
        /// </summary>
        /// <param name="modellid">For this modell.</param>
        /// <param name="extraid">With this extra.</param>
        /// <returns>íTrue on success.</returns>
        bool CreateModellsExtraData(int modellid, int extraid);

        /// <summary>
        /// Gets data.
        /// </summary>
        /// <returns>Returns data.</returns>
        StringBuilder GetExtrasData();

        /// <summary>
        /// Gets data.
        /// </summary>
        /// <returns>Returns data.</returns>
        StringBuilder GetBrandsData();

        /// <summary>
        /// Gets data.
        /// </summary>
        /// <returns>Returns data.</returns>
        StringBuilder GetModellsData();

        /// <summary>
        /// Checks if exists.
        /// </summary>
        /// <param name="id">Of this ID.</param>
        /// <returns>True if exists.</returns>
        bool CheckIfBrandExists(int id);

        /// <summary>
        /// Checks if exists.
        /// </summary>
        /// <param name="id">Of this ID.</param>
        /// <returns>True if exists.</returns>
        bool CheckIfModellExists(int id);

        /// <summary>
        /// Checks if exists.
        /// </summary>
        /// <param name="id">Of this ID.</param>
        /// <returns>True if exists.</returns>
        bool CheckIfExtraExists(int id);

        /// <summary>
        /// Deletes data.
        /// </summary>
        /// <param name="id">Of this ID.</param>
        /// <returns>True on success.</returns>
        bool RemoveExtraData(int id);

        /// <summary>
        /// Deletes data.
        /// </summary>
        /// <param name="id">Of this ID.</param>
        /// <returns>True on success.</returns>
        bool RemoveModellData(int id);

        /// <summary>
        /// Deletes data.
        /// </summary>
        /// <param name="id">Of this ID.</param>
        /// <returns>True on success.</returns>
        bool RemoveBrandData(int id);

        /// <summary>
        /// Deletes data.
        /// </summary>
        /// <param name="id">Of this ID.</param>
        /// <returns>True on success.</returns>
        bool RemoveModellsExtraData(int id);
    }
}
