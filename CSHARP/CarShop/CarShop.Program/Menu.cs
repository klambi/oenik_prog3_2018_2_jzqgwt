﻿//-----------------------------------------------------------------------
// <copyright file="Menu.cs" company="Klambauer Richárd">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Runtime.Remoting.Messaging;
    using System.Text;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Creates menu on call.
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// If called, its running.
        /// </summary>
        private bool running;

        /// <summary>
        /// Gets the index of the first starter tag and the length of it
        /// And the index of the end tag, than substrings.
        /// </summary>
        /// <param name="s">String input.</param>
        /// <param name="tag">Looking for this tag in s.</param>
        /// <returns>Returns data between tags.</returns>
        internal string TransformData(string s, string tag)
        {
            string startTag = "<" + tag + ">";
            int startIndex = s.IndexOf(startTag, StringComparison.Ordinal) + startTag.Length;
            int endIndex = s.IndexOf("</" + tag + ">", startIndex, StringComparison.Ordinal);
            return s.Substring(startIndex, endIndex - startIndex);
        }

        /// <summary>
        /// Puts menu on display.
        /// </summary>
        internal void MenuText()
        {
            Console.WriteLine("Válasszon menüpontot! (Üsse be a kiválasztott menüpont számát.)\n1. Táblák listázása\n2. Kiválasztott tábla adatainak listázása\n3. Rekord felvétele a kiválasztott táblába\n4. Rekord törlése a kiválaszott táblából\n5. Rekord frissítése a kiválasztott táblában\n6. Átlagos információk táblánként\n7. Random értékek generálása és mentése szerverről");
        }

        /// <summary>
        /// Loads and handles menu.
        /// </summary>
        internal void LoadMenu()
        {
            Logic.Logic logic = new Logic.Logic();
            this.running = true;
            this.MenuText();
            while (this.running)
            {
                string sel = Console.ReadLine();
                bool b = int.TryParse(sel, out int i);
                while (!b)
                {
                    Console.WriteLine("Nem létező opció / Nem működő opció! Lehetséges opciók listázása folyamatban...");
                    System.Threading.Thread.Sleep(500);
                    Console.Clear();
                    this.MenuText();
                    sel = Console.ReadLine();
                    b = int.TryParse(sel, out i);
                }

                try
                {
                    switch (i)
                    {
                        case 1:
                            {
                                Console.WriteLine("Elérhető adatbázisok listája:");
                                List<string> tablenames = new Logic.Logic().GetAllTableNames();
                                Console.WriteLine(string.Join(", ", tablenames));
                                break;
                            }

                        case 2:
                            {
                                List<string> tablenames = new Logic.Logic().GetAllTableNames();
                                Console.WriteLine("Mely táblából listázzak?");
                                string tablename = Console.ReadLine();
                                while (!tablenames.Contains(tablename))
                                {
                                    Console.WriteLine("Mely táblából listázzak?");
                                    tablename = Console.ReadLine();
                                }

                                Console.WriteLine(tablename + " tábla adatainak listázása folyamatban...");
                                Console.WriteLine();
                                if (tablename == "extras")
                                {
                                    string o = new Logic.Logic().GetExtrasData().ToString();
                                    Console.WriteLine(o);
                                }
                                else if (tablename == "modells")
                                {
                                    string o = new Logic.Logic().GetModellsData().ToString();
                                    Console.WriteLine(o);
                                }
                                else if (tablename == "brands")
                                {
                                    string o = new Logic.Logic().GetBrandsData().ToString();
                                    Console.WriteLine(o);
                                }
                                else if (tablename == "modellsextras")
                                {
                                    string o = new Logic.Logic().GetModelExtrasData().ToString();
                                    Console.WriteLine(o);
                                }

                                break;
                            }

                        case 3:
                            {
                                List<string> tablenames = new Logic.Logic().GetAllTableNames();
                                Console.WriteLine("Melyik táblát módosítsam?");
                                string tablename = Console.ReadLine();
                                while (!tablenames.Contains(tablename))
                                {
                                    Console.WriteLine("Melyik táblát módosítsam?");
                                    tablename = Console.ReadLine();
                                }

                                if (tablename == "extras")
                                {
                                    Console.WriteLine("Adjon meg egy kategóriát!");
                                    string category = Console.ReadLine();

                                    Console.WriteLine("Adjon meg egy nevet!");
                                    string name = Console.ReadLine();

                                    Console.WriteLine("Adjon meg egy árat!");
                                    string price = Console.ReadLine();
                                    bool b2 = int.TryParse(price, out int pricee);
                                    while (!b2)
                                    {
                                        Console.WriteLine("Adjon meg egy árat!");
                                        price = Console.ReadLine();
                                        b2 = int.TryParse(price, out pricee);
                                    }

                                    Console.WriteLine("Adja meg a színét!");
                                    string color = Console.ReadLine();

                                    Console.WriteLine("Többször használható termék? 0 - Nem, 1 - Igen");
                                    string canbeused = Console.ReadLine();
                                    bool b3 = int.TryParse(canbeused, out int canbeusedmorenumber);
                                    while (!b3 || (canbeusedmorenumber != 0 && canbeusedmorenumber != 1))
                                    {
                                        Console.WriteLine("Többször használható termék? 0 - Nem, 1 - Igen");
                                        canbeused = Console.ReadLine();
                                        b3 = int.TryParse(canbeused, out canbeusedmorenumber);
                                    }

                                    bool success = new Logic.Logic().CreateExtraData(category, name, pricee, color, canbeusedmorenumber == 1);
                                    string message = success ? "A bevitel sikerült." : "A bevitel nem sikerült.";
                                    Console.WriteLine(message);
                                }
                                else if (tablename == "modells")
                                {
                                    Console.WriteLine("Új modell felvételekür szükséges léteznie a márkának, így szükséges a márk azonosítója!");
                                    Console.WriteLine("Megvan már? I / N?");
                                    string yorno = Console.ReadLine();
                                    if (yorno != "n")
                                    {
                                    BrandDoesntExist:
                                        Console.WriteLine("Adja meg a márka ID-jét!");
                                        string idin = Console.ReadLine();
                                        bool b2 = int.TryParse(idin, out int id);
                                        while (!b2)
                                        {
                                            Console.WriteLine("Adja meg a márka ID-jét!");
                                            idin = Console.ReadLine();
                                            b2 = int.TryParse(idin, out id);
                                        }

                                        if (!new Logic.Logic().CheckIfBrandExists(id))
                                        {
                                            Console.WriteLine("Nem létezik az ID!");
                                            goto BrandDoesntExist;
                                        }

                                        Console.WriteLine("Adja meg a modell nevét!");
                                        string modell = Console.ReadLine();

                                        Console.WriteLine("Adja meg a modell megjelenésének dátumát! (yyyy-mm-dd)");
                                        string modelldate = Console.ReadLine();
                                        bool b3 = DateTime.TryParse(modelldate, out DateTime modelldate1);
                                        while (!b3)
                                        {
                                            Console.WriteLine("Adja meg a modell megjelenésének dátumát! (yyyy-mm-dd)");
                                            modelldate = Console.ReadLine();
                                            b3 = DateTime.TryParse(modelldate, out modelldate1);
                                        }

                                        Console.WriteLine("Adja meg a modellhez tartozó motor térfogatát!");
                                        string modellvolume = Console.ReadLine();
                                        bool b4 = int.TryParse(modellvolume, out int engine);
                                        while (!b4)
                                        {
                                            Console.WriteLine("Adja meg a modellhez tartozó motor térfogatát!");
                                            modellvolume = Console.ReadLine();
                                            b4 = int.TryParse(modellvolume, out engine);
                                        }

                                        Console.WriteLine("Adja meg a motor lóerejét!");
                                        string modellhorse = Console.ReadLine();
                                        bool b5 = int.TryParse(modellhorse, out int modellhorsey);
                                        while (!b5)
                                        {
                                            Console.WriteLine("Adja meg a motor lóerejét!");
                                            modellhorse = Console.ReadLine();
                                            b5 = int.TryParse(modellhorse, out modellhorsey);
                                        }

                                        Console.WriteLine("Adja meg a modell árát!");
                                        string modellprice = Console.ReadLine();
                                        bool b6 = int.TryParse(modellprice, out int modellpricey);
                                        while (!b6)
                                        {
                                            Console.WriteLine("Adja meg a modell árát!");
                                            modellprice = Console.ReadLine();
                                            b6 = int.TryParse(modellprice, out modellpricey);
                                        }

                                        bool success = new Logic.Logic().CreateModellData(id, modell, modelldate1, engine, modellhorsey, modellpricey);
                                        string message = success ? "A bevitel sikerült." : "A bevitel nem sikerült.";
                                        Console.WriteLine(message);
                                    }
                                }
                                else if (tablename == "brands")
                                {
                                    Console.WriteLine("Adjon meg a márka nevét!");
                                    string name = Console.ReadLine();

                                    Console.WriteLine("Adja meg az ország nevét, ahonnan a márka származik!");
                                    string country = Console.ReadLine();

                                    Console.WriteLine("Adja meg a márka weboldalát!");
                                    string web = Console.ReadLine();

                                    Console.WriteLine("Adja meg a cég alapításának évét!");
                                    string foundation = Console.ReadLine();
                                    bool b2 = int.TryParse(foundation, out int foundationy);
                                    while (!b2)
                                    {
                                        Console.WriteLine("Adja meg a cég alapításának évét!");
                                        foundation = Console.ReadLine();
                                        b2 = int.TryParse(foundation, out foundationy);
                                    }

                                    int found = int.Parse(foundationy.ToString());

                                    Console.WriteLine("Adja meg a cég éves forgalmát!");
                                    string annual = Console.ReadLine();
                                    bool b3 = int.TryParse(annual, out int annualy);
                                    while (!b3)
                                    {
                                        Console.WriteLine("Adja meg a cég éves forgalmát!");
                                        annual = Console.ReadLine();
                                        b3 = int.TryParse(annual, out annualy);
                                    }

                                    int ann = int.Parse(annualy.ToString());

                                    bool success = new Logic.Logic().CreateBrandData(name, country, web, found, ann);
                                    string message = success ? "A bevitel sikerült." : "A bevitel nem sikerült.";
                                    Console.WriteLine(message);
                                }
                                else if (tablename == "modellsextras")
                                {
                                AddExtraOnFail:
                                    Console.WriteLine("Adja meg, melyik modellhez adjunk hozzá extrát!");
                                    string modell = Console.ReadLine();
                                    bool b2 = int.TryParse(modell, out int modellid);
                                    while (!b2)
                                    {
                                        Console.WriteLine("Adja meg, melyik modellhez adjunk hozzá extrát!");
                                        modell = Console.ReadLine();
                                        b2 = int.TryParse(modell, out modellid);
                                    }

                                    Console.WriteLine("Adja meg a hozzáadandó extra ID-jét!");
                                    string extra = Console.ReadLine();
                                    bool b3 = int.TryParse(extra, out int extraid);
                                    while (!b3)
                                    {
                                        Console.WriteLine("Adja meg a hozzáadandó extra ID-jét!");
                                        extra = Console.ReadLine();
                                        b3 = int.TryParse(extra, out extraid);
                                    }

                                    if (!new Logic.Logic().DoesModellExist(modellid))
                                    {
                                        Console.WriteLine("Az ID nem létezik!");
                                        goto AddExtraOnFail;
                                    }

                                    if (!new Logic.Logic().DoesExtraExist(extraid))
                                    {
                                        Console.WriteLine("Az ID nem létezik!");
                                        goto AddExtraOnFail;
                                    }

                                    bool success = new Logic.Logic().CreateModellsExtraData(modellid, extraid);
                                    string message = success ? "A bevitel sikerült." : "A bevitel nem sikerült.";
                                    Console.WriteLine(message);
                                }
                                else
                                {
                                    Console.WriteLine("Valami nem sikerült!");
                                }

                                break;
                            }

                        case 4:
                            {
                                List<string> tablenames = new Logic.Logic().GetAllTableNames();
                                Console.WriteLine("Adja meg a tábla nevét, amiből törölni szeretne!");
                                string tablename = Console.ReadLine();
                                while (!tablenames.Contains(tablename))
                                {
                                    Console.WriteLine("Adja meg a tábla nevét, amiből törölni szeretne!");
                                    tablename = Console.ReadLine();
                                }

                                Console.WriteLine("Adja meg a törölni kívánt rekord azonosítóját!");
                                string rekordidin = Console.ReadLine();
                                bool b2 = int.TryParse(rekordidin, out int rekordid);
                                while (!b2)
                                {
                                    Console.WriteLine("Adja meg a törölni kívánt rekord azonosítóját!");
                                    rekordidin = Console.ReadLine();
                                    b2 = int.TryParse(rekordidin, out rekordid);
                                }

                                bool success;
                                if (tablename == "extras")
                                {
                                    success = new Logic.Logic().RemoveExtraData(rekordid);
                                }
                                else if (tablename == "brands")
                                {
                                    success = new Logic.Logic().RemoveBrandData(rekordid);
                                }
                                else if (tablename == "modells")
                                {
                                    success = new Logic.Logic().RemoveModellData(rekordid);
                                }
                                else
                                {
                                    Console.WriteLine("Nem létező rekord!");
                                    return;
                                }

                                string message = success ? "A törlés sikerült." : "A törlés nem sikerült.";
                                Console.WriteLine(message);
                                break;
                            }

                        case 5:
                            {
                                List<string> tablenames = new Logic.Logic().GetAllTableNames();
                                Console.WriteLine("Adja meg a frissíteni kívánt tábla nevét!");
                                string tablename = Console.ReadLine();
                                while (!tablenames.Contains(tablename))
                                {
                                    Console.WriteLine("Adja meg a frissíteni kívánt tábla nevét!");
                                    tablename = Console.ReadLine();
                                }

                            IDDoesNotExist:
                                Console.WriteLine("Adja meg a frissíteni kívánt rekord azonosítóját(ID)!");
                                string dbidin = Console.ReadLine();
                                bool b2 = int.TryParse(dbidin, out int dbid);
                                while (!b2)
                                {
                                    Console.WriteLine("Adja meg a frissíteni kívánt rekord azonosítóját(ID)!");
                                    dbidin = Console.ReadLine();
                                    b2 = int.TryParse(dbidin, out dbid);
                                }

                                if (tablename == "extras")
                                {
                                    if (!new Logic.Logic().CheckIfExtraExists(dbid))
                                    {
                                        Console.WriteLine("Nem létező ID a " + tablename + " táblában!");
                                        goto IDDoesNotExist;
                                    }

                                    Console.WriteLine("Adjon meg egy kategóriát!");
                                    string category = Console.ReadLine();

                                    Console.WriteLine("Adjon meg egy nevet!");
                                    string name = Console.ReadLine();

                                    Console.WriteLine("Adjon meg egy árat!");
                                    string price = Console.ReadLine();
                                    bool b21 = int.TryParse(price, out int pricee);
                                    while (!b21)
                                    {
                                        Console.WriteLine("Adjon meg egy árat!");
                                        price = Console.ReadLine();
                                        b21 = int.TryParse(price, out pricee);
                                    }

                                    Console.WriteLine("Adja meg a termék színét!");
                                    string color = Console.ReadLine();

                                    Console.WriteLine("Többször használható termék? 0 - Nem, 1 - Igen");
                                    string canbeused = Console.ReadLine();
                                    bool b3 = int.TryParse(canbeused, out int canbeusedmorenumber);
                                    while (!b3 || (canbeusedmorenumber != 0 && canbeusedmorenumber != 1))
                                    {
                                        Console.WriteLine("Többször használható termék? 0 - Nem, 1 - Igen");
                                        canbeused = Console.ReadLine();
                                        b3 = int.TryParse(canbeused, out canbeusedmorenumber);
                                    }

                                    bool success = new Logic.Logic().UpdateExtraData(dbid, category, name, pricee, color, canbeusedmorenumber == 1);
                                    string message = success ? "A frissítés sikerült." : "A frissítés nem sikerült.";
                                    Console.WriteLine(message);
                                }
                                else if (tablename == "brands")
                                {
                                    if (!new Logic.Logic().CheckIfBrandExists(dbid))
                                    {
                                        Console.WriteLine("Nem létező ID a " + tablename + " táblában!");
                                        goto IDDoesNotExist;
                                    }

                                    Console.WriteLine("Adja meg a márka nevét!");
                                    string name = Console.ReadLine();

                                    Console.WriteLine("Adja meg az ország nevét, ahonnan a márka származik!");
                                    string country = Console.ReadLine();

                                    Console.WriteLine("Adja meg a weboldalát a márkának!");
                                    string web = Console.ReadLine();

                                    Console.WriteLine("Adja meg a cég alapításának évét!");
                                    string foundation = Console.ReadLine();
                                    bool b22 = int.TryParse(foundation, out int foundationy);
                                    while (!b22)
                                    {
                                        Console.WriteLine("Adja meg a cég alapításának évét!");
                                        foundation = Console.ReadLine();
                                        b22 = int.TryParse(foundation, out foundationy);
                                    }

                                    Console.WriteLine("Adja meg a cég éves forgalmát!");
                                    string annual = Console.ReadLine();
                                    bool b3 = int.TryParse(annual, out int annualy);
                                    while (!b3)
                                    {
                                        Console.WriteLine("Adja meg a cég éves forgalmát!");
                                        annual = Console.ReadLine();
                                        b3 = int.TryParse(annual, out annualy);
                                    }

                                    bool success = new Logic.Logic().UpdateBrandData(dbid, name, country, web, foundationy, annualy);
                                    string message = success ? "A frissítés sikerült." : "A frissítés nem sikerült.";
                                    Console.WriteLine(message);
                                }
                                else if (tablename == "modells")
                                {
                                    if (!new Logic.Logic().CheckIfModellExists(dbid))
                                    {
                                        Console.WriteLine("Nem létező ID a " + tablename + " táblában!");
                                        goto IDDoesNotExist;
                                    }

                                BrandDoesntExist:
                                    Console.WriteLine("Adja meg a márka ID-jét!");
                                    string idin = Console.ReadLine();
                                    bool b22 = int.TryParse(idin, out int id);
                                    while (!b22)
                                    {
                                        Console.WriteLine("Adja meg a márka ID-jét!");
                                        idin = Console.ReadLine();
                                        b22 = int.TryParse(idin, out id);
                                    }

                                    if (!new Logic.Logic().CheckIfBrandExists(id))
                                    {
                                        Console.WriteLine("Nem létező a ID!");
                                        goto BrandDoesntExist;
                                    }

                                    Console.WriteLine("Adja meg a modell nevét!");
                                    string modell = Console.ReadLine();

                                    Console.WriteLine("Adja meg a modell megjelenésének dátumát! (yyyy-mm-dd)");
                                    string modelldate = Console.ReadLine();
                                    bool b3 = DateTime.TryParse(modelldate, out DateTime modelldate1);
                                    while (!b3)
                                    {
                                        Console.WriteLine("Adja meg a modell megjelenésének dátumát! (yyyy-mm-dd)");
                                        modelldate = Console.ReadLine();
                                        b3 = DateTime.TryParse(modelldate, out modelldate1);
                                    }

                                    Console.WriteLine("Adja meg a modellhez tartozó motor térfogatát!");
                                    string modellvolume = Console.ReadLine();
                                    bool b4 = int.TryParse(modellvolume, out int engine);
                                    while (!b4)
                                    {
                                        Console.WriteLine("Adja meg a modellhez tartozó motor térfogatát!");
                                        modellvolume = Console.ReadLine();
                                        b4 = int.TryParse(modellvolume, out engine);
                                    }

                                    Console.WriteLine("Adja meg a motor lóerejét!");
                                    string modellhorse = Console.ReadLine();
                                    bool b5 = int.TryParse(modellhorse, out int modellhorsey);
                                    while (!b5)
                                    {
                                        Console.WriteLine("Adja meg a motor lóerejét!");
                                        modellhorse = Console.ReadLine();
                                        b5 = int.TryParse(modellhorse, out modellhorsey);
                                    }

                                    Console.WriteLine("Adja meg a modell árát!");
                                    string modellprice = Console.ReadLine();
                                    bool b6 = int.TryParse(modellprice, out int modellpricey);
                                    while (!b6)
                                    {
                                        Console.WriteLine("Adja meg a modell árát!");
                                        modellprice = Console.ReadLine();
                                        b6 = int.TryParse(modellprice, out modellpricey);
                                    }

                                    bool success = new Logic.Logic().UpdateModellData(dbid, id, modell, modelldate1, engine, modellhorsey, modellpricey);
                                    string message = success ? "A frissítés sikerült." : "A frissítés nem sikerült.";
                                    Console.WriteLine(message);
                                }
                                else if (tablename == "modellsextras")
                                {
                                    Console.WriteLine("Adja meg a törölni kívánt modell extájának azonosítóját!");
                                    string extra = Console.ReadLine();
                                    bool b3 = int.TryParse(extra, out int extraid);
                                    while (!b3)
                                    {
                                        Console.WriteLine("Adja meg a törölni kívánt modell extájának azonosítóját!");
                                        extra = Console.ReadLine();
                                        b3 = int.TryParse(extra, out extraid);
                                    }

                                    bool success = new Logic.Logic().RemoveModellsExtraData(extraid);
                                    string message = success ? "A törlés sikerült." : "A törlés nem sikerült.";
                                    Console.WriteLine(message);
                                }
                                else
                                {
                                    Console.WriteLine("Valami nem sikerült!");
                                    return;
                                }

                                break;
                            }

                        case 6:
                            {
                                List<string> tablenames = logic.GetAllTableNames();
                                Console.WriteLine("Adjon meg egy létező táblát, amiben átlagolni szeretne!");
                                string tablename = Console.ReadLine();
                                while (!tablenames.Contains(tablename) || tablename == "modellsextras")
                                {
                                    if (tablename == "modellsextras")
                                    {
                                        Console.WriteLine("Eben a táblában nem tudok átlagolni!");
                                    }

                                    Console.WriteLine("Adjon meg egy létező táblát, amiben átlagolni szeretne!");
                                    tablename = Console.ReadLine();
                                }

                                if (tablename == "extras")
                                {
                                    Console.WriteLine("Adjon meg egy színt!");
                                    Console.WriteLine("Ha a rendszer nem talál adatot a kapott színnel, akkor -1-et fog visszadobni.");
                                    string color = Console.ReadLine();
                                    double avg = logic.GetAverageForExtraColor(color);
                                    Console.WriteLine("Átlagos darabszám " + color + " színnel: " + avg);
                                }
                                else if (tablename == "brands")
                                {
                                BrandDoesntExist:
                                    Console.WriteLine("Adja meg a márka azonosítóját!");
                                    Console.Write("Márka azonosítója: ");
                                    string idin = Console.ReadLine();
                                    bool b2 = int.TryParse(idin, out int id);
                                    while (!b2)
                                    {
                                        Console.WriteLine("Adja meg a márka azonosítóját!");
                                        idin = Console.ReadLine();
                                        b2 = int.TryParse(idin, out id);
                                    }

                                    if (!logic.CheckIfBrandExists(id))
                                    {
                                        Console.WriteLine("Nem létező márka azonosító!");
                                        goto BrandDoesntExist;
                                    }

                                    double avg = logic.GetAverageAmountForBrand(id);
                                    Console.WriteLine("Márka átlagos modellszáma: " + avg);
                                }
                                else if (tablename == "modells")
                                {
                                    Console.WriteLine("Adjon meg egy országot!");
                                    Console.WriteLine("Ha a rendszer nem talál adatot a kapott országgal, akkor -1-et fog visszadobni.");
                                    Console.Write("Ország: ");
                                    string country = Console.ReadLine();
                                    double avg = logic.GetAverageAmountForBrandByCountry(country);
                                    Console.WriteLine("Átlagos márkák száma " + country + "-ban/ben: " + avg);
                                }
                                else
                                {
                                    Console.WriteLine("Nem létező tábla!");
                                    return;
                                }

                                break;
                            }

                        case 7:
                            {
                                WebClient client = new WebClient();
                                Console.WriteLine("FIGYELJEN RÁ, HOGY EZEN MENÜPONT ALATT NEM TUD MEGLÉVŐ REKORDOT MÓDOSÍTANI!");
                                Console.WriteLine("ADATBEKÉRÉS UTÁN LÉTEZŐ ID MEGADÁSAKOR HIBÁT KAP!");
                                Console.WriteLine("Adjon meg egy minimális árat!");
                                string pricemin = Console.ReadLine();

                                Console.WriteLine("Adjon meg egy maximális árat!");
                                string pricemax = Console.ReadLine();

                                Console.WriteLine("Adjon meg egy minimális évet!");
                                string yearmin = Console.ReadLine();

                                Console.WriteLine("Adjon meg egy maximális évet!");
                                string yearmax = Console.ReadLine();

                                Console.WriteLine("Adjon meg egy minimális motortérfogatot!");
                                string enginemin = Console.ReadLine();

                                Console.WriteLine("Adjon meg egy maximális motortérfogatot!");
                                string enginemax = Console.ReadLine();

                                Console.WriteLine("Adjon meg egy minimális lóerő!");
                                string hpmin = Console.ReadLine();

                                Console.WriteLine("Adjon meg egy maximális lóerő!");
                                string hpmax = Console.ReadLine();

                                client.Encoding = Encoding.UTF8;
                                string response = client.DownloadString(Program.LocationURL + "?release=" + yearmin + "-" + yearmax + "&engine=" + enginemin + "-" + enginemax + "&hp=" + hpmin + "-" + hpmax + "&price=" + pricemin + "-" + pricemax);

                                string fullresponse = this.TransformData(response, "body").Trim();
                                if (fullresponse.Contains("Hiba!"))
                                {
                                    Console.WriteLine("A következő lekérdezés hibás: " + fullresponse);
                                }
                                else
                                {
                                    Console.WriteLine("Kapott válasz: " + fullresponse);
                                    ModellFromServer modell = JsonConvert.DeserializeObject<ModellFromServer>(fullresponse);

                                    int release = int.Parse(modell.Release);
                                    int engine = int.Parse(modell.Engine);
                                    int hp = int.Parse(modell.HP);
                                    int price = int.Parse(modell.Price);

                                BrandDoesntExist:
                                    Console.WriteLine("Adja meg a márka ID-jét!");
                                    string idin = Console.ReadLine();
                                    bool b2 = int.TryParse(idin, out int brandid);
                                    while (!b2)
                                    {
                                        Console.WriteLine("Adja meg a márka ID-jét!");
                                        idin = Console.ReadLine();
                                        b2 = int.TryParse(idin, out brandid);
                                    }

                                    if (!new Logic.Logic().CheckIfBrandExists(brandid))
                                    {
                                        Console.WriteLine("Nem létező a ID!");
                                        goto BrandDoesntExist;
                                    }

                                    Console.WriteLine("Adjon meg egy modell nevet a rekord elmentéséhez!");
                                    string modellname = Console.ReadLine();

                                    bool success = new Logic.Logic().CreateModellData(brandid, modellname, new DateTime(release, 1, 10), engine, hp, price);
                                    string message = success ? "A bevitel a szerverről sikerült." : "A bevitel a szerverről nem sikerült.";
                                    Console.WriteLine(message);
                                }

                                break;
                            }

                        default:
                            {
                                Console.WriteLine("Nem létező opció / Nem működő opció! Lehetséges opciók listázása folyamatban...");
                                break;
                            }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Hibába ütköztünk: " + e.Message);
                }
            }
        }
    }
}
