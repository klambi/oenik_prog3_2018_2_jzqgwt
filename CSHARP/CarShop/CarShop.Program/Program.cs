﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Klambauer Richárd">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Program
{
    /// <summary>
    /// This is our main class, everything is called from here.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Location URL of web end.
        /// </summary>
        public const string LocationURL = "http://localhost:8080/CarShopWebProject/";

        /// <summary>
        /// Static member of menu instance.
        /// </summary>
        private static Menu menu;

        /// <summary>
        /// Main program code of the project or loading methods.
        /// </summary>
        /// <param name="args">Basic parameters of a program main.</param>
        public static void Main(string[] args)
        {
            bool successOnStart;
            try
            {
                menu = new Menu();
                successOnStart = true;
            }
            catch (System.Exception)
            {
                successOnStart = false;
                System.Console.WriteLine("Nem sikerült csatlakozni az adatbázishoz...");
                System.Console.ReadLine();
            }

            if (successOnStart)
            {
                menu.LoadMenu();
            }
        }
    }
}