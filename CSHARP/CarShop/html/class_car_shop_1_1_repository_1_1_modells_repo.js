var class_car_shop_1_1_repository_1_1_modells_repo =
[
    [ "CheckIfModellIdExist", "class_car_shop_1_1_repository_1_1_modells_repo.html#a613a3f2c3eeb30244e6eca1fcc08e529", null ],
    [ "Create", "class_car_shop_1_1_repository_1_1_modells_repo.html#aaefd82ee034178198111934c024a1a7c", null ],
    [ "GetAll", "class_car_shop_1_1_repository_1_1_modells_repo.html#a83513904907c90560895a675324de5e1", null ],
    [ "GetAverageAmountForBrand", "class_car_shop_1_1_repository_1_1_modells_repo.html#afbece3a05104ed6f9095cc47444e6743", null ],
    [ "GetTableContents", "class_car_shop_1_1_repository_1_1_modells_repo.html#a00ad664c0dca4dc88abdc506df7f8f5f", null ],
    [ "LoadModells", "class_car_shop_1_1_repository_1_1_modells_repo.html#a361796c8abf39a2265e739562e4da884", null ],
    [ "Remove", "class_car_shop_1_1_repository_1_1_modells_repo.html#a39cb05e609b91dd0a8ba17ad48d055cc", null ],
    [ "Update", "class_car_shop_1_1_repository_1_1_modells_repo.html#aa925d65f0e9c2cb40ff8ddb524fc4651", null ]
];