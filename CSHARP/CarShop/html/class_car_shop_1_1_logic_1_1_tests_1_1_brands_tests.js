var class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests =
[
    [ "BrandExistCallCheck", "class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests.html#a7cfda39e01e9b00130dd071d9333f24d", null ],
    [ "BrandGetAllCountIsNotZero", "class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests.html#a1afc6077843f8e116ef793cc941c4fab", null ],
    [ "BrandGetAllCountIsZero", "class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests.html#ae5ba5fa2de07cdc23811ddccef059565", null ],
    [ "CreateBrandCallCheck", "class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests.html#a406ea4f9d4da2e1fe561f431ff90fd25", null ],
    [ "DeleteBrandCallCheck", "class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests.html#a160cbfafa4a35657974aed0450d0ae30", null ],
    [ "GetAllBrandCallCheck", "class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests.html#ae2e97097ecd8483d86ab7b80175d2fa2", null ],
    [ "GetBrandTableContentCallCheck", "class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests.html#a0e9f67867c5d7a466219cdef3410ca02", null ],
    [ "UpdateBrandCallCheck", "class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests.html#a5077ee21dc6e784baba07be660a0d6af", null ]
];