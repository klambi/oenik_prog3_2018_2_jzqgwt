var class_car_shop_1_1_repository_1_1_brands_repo =
[
    [ "CheckIfBrandExists", "class_car_shop_1_1_repository_1_1_brands_repo.html#a6405e826f91047e3eb822a32c30abf10", null ],
    [ "Create", "class_car_shop_1_1_repository_1_1_brands_repo.html#a25488382f6f5a0ed93eb34b09f1791f1", null ],
    [ "GetAll", "class_car_shop_1_1_repository_1_1_brands_repo.html#aceae0fcc7521f5500e107281f51a3cb9", null ],
    [ "GetAverageForBrandAmountByCountry", "class_car_shop_1_1_repository_1_1_brands_repo.html#ad0783f9d0e8500760f2441bd8ff07bfd", null ],
    [ "GetTableContents", "class_car_shop_1_1_repository_1_1_brands_repo.html#a0dd330e5161cdbd1daa29f4b679d8267", null ],
    [ "LoadBrands", "class_car_shop_1_1_repository_1_1_brands_repo.html#afe197302ea3bd85f07e29be668ab08b2", null ],
    [ "Remove", "class_car_shop_1_1_repository_1_1_brands_repo.html#adc4e2e30d5b7402b043477a6406f361b", null ],
    [ "Update", "class_car_shop_1_1_repository_1_1_brands_repo.html#a2ded684f2ae266244e596e31f5802fdb", null ]
];