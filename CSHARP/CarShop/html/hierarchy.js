var hierarchy =
[
    [ "CarShop.Data.brands", "class_car_shop_1_1_data_1_1brands.html", null ],
    [ "CarShop.Logic.Tests.BrandsTests", "class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests.html", null ],
    [ "DbContext", null, [
      [ "CarShop.Data.dbEntities", "class_car_shop_1_1_data_1_1db_entities.html", null ]
    ] ],
    [ "CarShop.Data.extras", "class_car_shop_1_1_data_1_1extras.html", null ],
    [ "CarShop.Logic.Tests.ExtrasTests", "class_car_shop_1_1_logic_1_1_tests_1_1_extras_tests.html", null ],
    [ "CarShop.Repository.Helper", "class_car_shop_1_1_repository_1_1_helper.html", null ],
    [ "CarShop.Logic.ILogic", "interface_car_shop_1_1_logic_1_1_i_logic.html", [
      [ "CarShop.Logic.Logic", "class_car_shop_1_1_logic_1_1_logic.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< T >", "interface_car_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "CarShop.Repository.IRepository< brands >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.BrandsRepo", "class_car_shop_1_1_repository_1_1_brands_repo.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< extras >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ExtrasRepo", "class_car_shop_1_1_repository_1_1_extras_repo.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< modells >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ModellsRepo", "class_car_shop_1_1_repository_1_1_modells_repo.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< modellsextras >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ModellsExtrasRepo", "class_car_shop_1_1_repository_1_1_modells_extras_repo.html", null ]
    ] ],
    [ "CarShop.Program.Menu", "class_car_shop_1_1_program_1_1_menu.html", null ],
    [ "CarShop.Program.ModellFromServer", "class_car_shop_1_1_program_1_1_modell_from_server.html", null ],
    [ "CarShop.Data.modells", "class_car_shop_1_1_data_1_1modells.html", null ],
    [ "CarShop.Data.modellsextras", "class_car_shop_1_1_data_1_1modellsextras.html", null ],
    [ "CarShop.Logic.Tests.ModellsExtrasTests", "class_car_shop_1_1_logic_1_1_tests_1_1_modells_extras_tests.html", null ],
    [ "CarShop.Logic.Tests.ModellsTests", "class_car_shop_1_1_logic_1_1_tests_1_1_modells_tests.html", null ]
];