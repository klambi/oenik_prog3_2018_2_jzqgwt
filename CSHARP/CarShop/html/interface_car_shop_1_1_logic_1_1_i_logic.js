var interface_car_shop_1_1_logic_1_1_i_logic =
[
    [ "CheckIfBrandExists", "interface_car_shop_1_1_logic_1_1_i_logic.html#a352b7f8d250b1e4b95a1c27cec75281a", null ],
    [ "CheckIfExtraExists", "interface_car_shop_1_1_logic_1_1_i_logic.html#a43a41b663b63abeaf0f935ee6a3fa8fb", null ],
    [ "CheckIfModellExists", "interface_car_shop_1_1_logic_1_1_i_logic.html#ae08649329c130c89be975c7bffde18a8", null ],
    [ "CreateBrandData", "interface_car_shop_1_1_logic_1_1_i_logic.html#adcf54950fdf7bae9b44d720bd89379e9", null ],
    [ "CreateExtraData", "interface_car_shop_1_1_logic_1_1_i_logic.html#a247ac77be0436d3c28d8fc5e8caf6967", null ],
    [ "CreateModellData", "interface_car_shop_1_1_logic_1_1_i_logic.html#aebf090e2ede0514e3202004c7a1639e2", null ],
    [ "CreateModellsExtraData", "interface_car_shop_1_1_logic_1_1_i_logic.html#a5b400d0f9c5562313e1c62387141be25", null ],
    [ "GetAllTableNames", "interface_car_shop_1_1_logic_1_1_i_logic.html#aee2a118a9a867dbc77901298cf112e02", null ],
    [ "GetBrandsData", "interface_car_shop_1_1_logic_1_1_i_logic.html#a3fad1ae2fa55b77fedf2688e9915f376", null ],
    [ "GetExtrasData", "interface_car_shop_1_1_logic_1_1_i_logic.html#a5ad3fa8e566e99e43a70a5e9f7ffb114", null ],
    [ "GetModellsData", "interface_car_shop_1_1_logic_1_1_i_logic.html#a55c5c8359828dd41217686ca5127260f", null ],
    [ "RemoveBrandData", "interface_car_shop_1_1_logic_1_1_i_logic.html#a48826fab3f7234de088227c550f893f0", null ],
    [ "RemoveExtraData", "interface_car_shop_1_1_logic_1_1_i_logic.html#afbe0696edc7b7996e0a1aacdb4e3a6a9", null ],
    [ "RemoveModellData", "interface_car_shop_1_1_logic_1_1_i_logic.html#a6f74d30cf995b27c89e95d19863bf57c", null ],
    [ "RemoveModellsExtraData", "interface_car_shop_1_1_logic_1_1_i_logic.html#a9e72f7622c4a0c2fa7caf2c9f1a4d6d2", null ]
];