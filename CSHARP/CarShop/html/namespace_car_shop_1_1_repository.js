var namespace_car_shop_1_1_repository =
[
    [ "BrandsRepo", "class_car_shop_1_1_repository_1_1_brands_repo.html", "class_car_shop_1_1_repository_1_1_brands_repo" ],
    [ "ExtrasRepo", "class_car_shop_1_1_repository_1_1_extras_repo.html", "class_car_shop_1_1_repository_1_1_extras_repo" ],
    [ "Helper", "class_car_shop_1_1_repository_1_1_helper.html", "class_car_shop_1_1_repository_1_1_helper" ],
    [ "IRepository", "interface_car_shop_1_1_repository_1_1_i_repository.html", "interface_car_shop_1_1_repository_1_1_i_repository" ],
    [ "ModellsExtrasRepo", "class_car_shop_1_1_repository_1_1_modells_extras_repo.html", "class_car_shop_1_1_repository_1_1_modells_extras_repo" ],
    [ "ModellsRepo", "class_car_shop_1_1_repository_1_1_modells_repo.html", "class_car_shop_1_1_repository_1_1_modells_repo" ]
];