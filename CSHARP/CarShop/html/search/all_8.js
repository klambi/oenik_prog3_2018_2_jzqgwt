var searchData=
[
  ['loadbrands',['LoadBrands',['../class_car_shop_1_1_repository_1_1_brands_repo.html#afe197302ea3bd85f07e29be668ab08b2',1,'CarShop::Repository::BrandsRepo']]],
  ['loadextras',['LoadExtras',['../class_car_shop_1_1_repository_1_1_extras_repo.html#a349bff6ea44fc27231024d88b832f972',1,'CarShop::Repository::ExtrasRepo']]],
  ['loadmodells',['LoadModells',['../class_car_shop_1_1_repository_1_1_modells_repo.html#a361796c8abf39a2265e739562e4da884',1,'CarShop::Repository::ModellsRepo']]],
  ['loadmodellsextras',['LoadModellsExtras',['../class_car_shop_1_1_repository_1_1_modells_extras_repo.html#acf15e9e84315eabf489da02ac731c211',1,'CarShop::Repository::ModellsExtrasRepo']]],
  ['logic',['Logic',['../class_car_shop_1_1_logic_1_1_logic.html',1,'CarShop.Logic.Logic'],['../class_car_shop_1_1_logic_1_1_logic.html#af006d6f092e7d6adabc05cb8e546d829',1,'CarShop.Logic.Logic.Logic()']]],
  ['license',['LICENSE',['../md__car_shop_packages__newtonsoft_8_json_812_80_81__l_i_c_e_n_s_e.html',1,'']]]
];
