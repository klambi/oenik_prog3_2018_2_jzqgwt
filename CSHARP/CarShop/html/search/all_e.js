var searchData=
[
  ['update',['Update',['../class_car_shop_1_1_repository_1_1_brands_repo.html#a2ded684f2ae266244e596e31f5802fdb',1,'CarShop.Repository.BrandsRepo.Update()'],['../class_car_shop_1_1_repository_1_1_extras_repo.html#a48dc76710bea2c10949f180b24d933b3',1,'CarShop.Repository.ExtrasRepo.Update()'],['../class_car_shop_1_1_repository_1_1_modells_repo.html#aa925d65f0e9c2cb40ff8ddb524fc4651',1,'CarShop.Repository.ModellsRepo.Update()']]],
  ['updatebrandcallcheck',['UpdateBrandCallCheck',['../class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests.html#a5077ee21dc6e784baba07be660a0d6af',1,'CarShop::Logic::Tests::BrandsTests']]],
  ['updatebranddata',['UpdateBrandData',['../class_car_shop_1_1_logic_1_1_logic.html#abc5cfc7768ec3f93b36675c3259acc1c',1,'CarShop::Logic::Logic']]],
  ['updateextracallcheck',['UpdateExtraCallCheck',['../class_car_shop_1_1_logic_1_1_tests_1_1_extras_tests.html#a0d516d8835061477017d264bdd7978ab',1,'CarShop::Logic::Tests::ExtrasTests']]],
  ['updateextradata',['UpdateExtraData',['../class_car_shop_1_1_logic_1_1_logic.html#a427230e7be92372715cd39a8b657c28d',1,'CarShop::Logic::Logic']]],
  ['updatemodellcallcheck',['UpdateModellCallCheck',['../class_car_shop_1_1_logic_1_1_tests_1_1_modells_tests.html#ab6cb77638d1dcdd4d38f80650954e670',1,'CarShop::Logic::Tests::ModellsTests']]],
  ['updatemodelldata',['UpdateModellData',['../class_car_shop_1_1_logic_1_1_logic.html#a243a265d045335601e26f21ba7dd1627',1,'CarShop::Logic::Logic']]],
  ['url',['url',['../class_car_shop_1_1_data_1_1brands.html#aca3338ad462d87b7c23d9a35fbf4700f',1,'CarShop::Data::brands']]]
];
