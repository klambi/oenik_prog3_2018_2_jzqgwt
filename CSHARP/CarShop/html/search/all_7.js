var searchData=
[
  ['id',['Id',['../class_car_shop_1_1_data_1_1brands.html#a34c9e51a5fc7edfe49f572fe4296efaa',1,'CarShop.Data.brands.Id()'],['../class_car_shop_1_1_data_1_1extras.html#a9a84622cd67257e8ca19c8d8c37740d6',1,'CarShop.Data.extras.Id()'],['../class_car_shop_1_1_data_1_1modells.html#ad8e780f4767e81fa27b2db9cb9a6e53f',1,'CarShop.Data.modells.Id()'],['../class_car_shop_1_1_data_1_1modellsextras.html#a56174a5d5870218e5d1042faeb2b4aff',1,'CarShop.Data.modellsextras.Id()']]],
  ['ilogic',['ILogic',['../interface_car_shop_1_1_logic_1_1_i_logic.html',1,'CarShop::Logic']]],
  ['income',['income',['../class_car_shop_1_1_data_1_1brands.html#aa8009d033f490301d55e65c87251a6cc',1,'CarShop::Data::brands']]],
  ['irepository',['IRepository',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20brands_20_3e',['IRepository&lt; brands &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20extras_20_3e',['IRepository&lt; extras &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20modells_20_3e',['IRepository&lt; modells &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]],
  ['irepository_3c_20modellsextras_20_3e',['IRepository&lt; modellsextras &gt;',['../interface_car_shop_1_1_repository_1_1_i_repository.html',1,'CarShop::Repository']]]
];
