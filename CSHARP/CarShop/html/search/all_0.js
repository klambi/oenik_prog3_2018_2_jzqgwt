var searchData=
[
  ['brand_5fid',['brand_id',['../class_car_shop_1_1_data_1_1modells.html#a02c70ab31a7ff4860616519879fc3265',1,'CarShop::Data::modells']]],
  ['brandexistcallcheck',['BrandExistCallCheck',['../class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests.html#a7cfda39e01e9b00130dd071d9333f24d',1,'CarShop::Logic::Tests::BrandsTests']]],
  ['brandgetallcountisnotzero',['BrandGetAllCountIsNotZero',['../class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests.html#a1afc6077843f8e116ef793cc941c4fab',1,'CarShop::Logic::Tests::BrandsTests']]],
  ['brandgetallcountiszero',['BrandGetAllCountIsZero',['../class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests.html#ae5ba5fa2de07cdc23811ddccef059565',1,'CarShop::Logic::Tests::BrandsTests']]],
  ['brands',['brands',['../class_car_shop_1_1_data_1_1brands.html',1,'CarShop.Data.brands'],['../class_car_shop_1_1_data_1_1db_entities.html#ae468a28bbddbc0f3d6ef2b5ffa2a30e3',1,'CarShop.Data.dbEntities.brands()']]],
  ['brandsrepo',['BrandsRepo',['../class_car_shop_1_1_repository_1_1_brands_repo.html',1,'CarShop.Repository.BrandsRepo'],['../class_car_shop_1_1_repository_1_1_helper.html#a904800990da7cabf1b5cdac84a64d2bc',1,'CarShop.Repository.Helper.BrandsRepo()']]],
  ['brandstests',['BrandsTests',['../class_car_shop_1_1_logic_1_1_tests_1_1_brands_tests.html',1,'CarShop::Logic::Tests']]]
];
