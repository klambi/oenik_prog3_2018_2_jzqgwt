var class_car_shop_1_1_repository_1_1_extras_repo =
[
    [ "CheckIfExtraIdExist", "class_car_shop_1_1_repository_1_1_extras_repo.html#a24fcfd4f81d9479071c6e5c64c31ce04", null ],
    [ "Create", "class_car_shop_1_1_repository_1_1_extras_repo.html#a4600ce63856c3a8442b5de4d18db9ae4", null ],
    [ "GetAll", "class_car_shop_1_1_repository_1_1_extras_repo.html#ac5286d55b9ab9ff404ca8e6f93c2c1a9", null ],
    [ "GetAverageForExtraColor", "class_car_shop_1_1_repository_1_1_extras_repo.html#aed7e8a49474f4205b8f35df3ca591951", null ],
    [ "GetTableContents", "class_car_shop_1_1_repository_1_1_extras_repo.html#a209a5f4de04496969d463d9d0bb807f7", null ],
    [ "LoadExtras", "class_car_shop_1_1_repository_1_1_extras_repo.html#a349bff6ea44fc27231024d88b832f972", null ],
    [ "Remove", "class_car_shop_1_1_repository_1_1_extras_repo.html#af67179b5dae2fefce02b4482318d7d78", null ],
    [ "Update", "class_car_shop_1_1_repository_1_1_extras_repo.html#a48dc76710bea2c10949f180b24d933b3", null ]
];