﻿CREATE TABLE [dbo].[brands]
(
	[Id]					INT NOT NULL PRIMARY KEY, 
    [name]					VARCHAR(20) NULL, 
    [country]				VARCHAR(30) NULL, 
    [url]					VARCHAR(30) NULL, 
    [foundation]			INT NULL, 
    [income]				INT NULL
);

CREATE TABLE [dbo].[extras] (
    [Id]					INT          NOT NULL,
    [category]				VARCHAR (50) NULL,
    [name]					VARCHAR (50) NULL,
    [price]					INT          NULL,
    [color]					VARCHAR (50) NULL,
    [reusable]				TINYINT      NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[modells]
(
	[Id]					INT NOT NULL PRIMARY KEY, 
    [brand_id]				INT NULL,
    [name]					VARCHAR(50) NULL, 
    [date]					DATE NULL, 
    [engineinfo]			INT NULL, 
    [hp]					INT NULL, 
    [price]					INT NULL,
	
);

CREATE TABLE [dbo].[modellsextras]
(
	[Id]					INT NOT NULL PRIMARY KEY, 
    [modellid]				INT NULL, 
    [extraid]				INT NULL,
	
);

INSERT INTO brands VALUES('Alpha Romeo', 'Olaszország', 'https://alfaromeo.hu', 1910, 85691);
INSERT INTO brands VALUES('Opel', 'Németország', 'https://opel.hu', 1862, 344848);
INSERT INTO brands VALUES('BMW', 'Németország', 'https://bmw.hu', 1916, 827137);
INSERT INTO brands VALUES('Citroen', 'Franciaország', 'https://citroen.hu', 1919, 569728);
INSERT INTO brands VALUES('AUDI', 'Németország', 'https://audi.hu', 1964, 826370);



INSERT INTO extras VALUES(1, 'Ablak', 'Árnyékoló/Sötétítő fólia', 15000, 'Fekete', 0);
INSERT INTO extras VALUES(2, 'Ablak', 'Esővízterelő', 5000, 'Fekete', 1);
INSERT INTO extras VALUES(3, 'Matrica', 'Smoke&Charm', 1500, 'Fehér', 0);
INSERT INTO extras VALUES(4, 'Matrica', 'Opel, sose kop el', 1500, 'Fehér', 0);
INSERT INTO extras VALUES(5, 'Légterelő', 'Alacsony, csomagtartóra szerelhető', 50000, 'Fekete', 1);
INSERT INTO extras VALUES(6, 'Légterelő', 'Magas, csomagtartóra szerelhető', 40000, 'Barna', 1);
INSERT INTO extras VALUES(7, 'Vezérlés', 'Tolató Radar', 10000, 'Nincs', 1);
INSERT INTO extras VALUES(8, 'Vezérlés', 'Generátor', 25000, 'Nincs', 0);
INSERT INTO extras VALUES(9, 'Antenna', 'Nomrál antenna', 4000, 'Nincs', 1);
INSERT INTO extras VALUES(10, 'Antenna', 'Luxus antenna, 5x10x5cm', 6000, 'Nincs', 1);


INSERT INTO modells VALUES(1, '4C', '2016-01-01', 1500, 250, 35000000);
INSERT INTO modells VALUES(2, 'ADAM', '2011-03-27', 1490, 110, 200000);
INSERT INTO modells VALUES(2, 'CASCADA', '2015-04-04', 1560, 150, 3500000);
INSERT INTO modells VALUES(2, 'CORSA', '2007-12-24', 1100, 70, 650000);
INSERT INTO modells VALUES(3, 'M5', '2014-03-21', 1700, 170, 5000000);
INSERT INTO modells VALUES(4, 'C8', '2011-09-12', 1354, 130, 6200000);
INSERT INTO modells VALUES(4, 'DS3', '2015-05-12', 1300, 150, 4500000);
INSERT INTO modells VALUES(5, 'A8', '2015-11-03', 1750, 170, 5000000);


INSERT INTO modellsextras VALUES(1, 1, 1);
INSERT INTO modellsextras VALUES(2, 1, 2);
INSERT INTO modellsextras VALUES(3, 1, 3);

INSERT INTO modellsextras VALUES(4, 5, 4);
INSERT INTO modellsextras VALUES(5, 5, 5);
INSERT INTO modellsextras VALUES(6, 5, 6);

INSERT INTO modellsextras VALUES(7, 7, 1);
INSERT INTO modellsextras VALUES(8, 7, 4);
INSERT INTO modellsextras VALUES(9, 7, 3);
INSERT INTO modellsextras VALUES(10, 7, 9);

INSERT INTO modellsextras VALUES(11, 8, 9);
INSERT INTO modellsextras VALUES(12, 8, 8);
INSERT INTO modellsextras VALUES(13, 8, 10);
INSERT INTO modellsextras VALUES(14, 8, 1);
INSERT INTO modellsextras VALUES(15, 8, 1);
INSERT INTO modellsextras VALUES(16, 8, 1);
INSERT INTO modellsextras VALUES(17, 8, 6);