//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CarShop.Data
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// <autogenerated>No comment.</autogenerated>
    /// </summary>
    public partial class extras
    {
        /// <summary>
        /// <autogenerated>Gets or sets still no comment.</autogenerated>
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// <autogenerated>Gets or sets still no comment.</autogenerated>
        /// </summary>
        public string category { get; set; }

        /// <summary>
        /// <autogenerated>Gets or sets still no comment.</autogenerated>
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// <autogenerated>Gets or sets still no comment.</autogenerated>
        /// </summary>
        public Nullable<int> price { get; set; }

        /// <summary>
        /// <autogenerated>Gets or sets still no comment.</autogenerated>
        /// </summary>
        public string color { get; set; }

        /// <summary>
        /// <autogenerated>Gets or sets still no comment.</autogenerated>
        /// </summary>
        public Nullable<byte> reusable { get; set; }
    }
}
