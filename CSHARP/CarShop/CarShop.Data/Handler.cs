﻿//-----------------------------------------------------------------------
// <copyright file="Handler.cs" company="Klambauer Richárd">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Data
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// This class handles the main dataflow in the database, this is the lowest layer.
    /// </summary>
    public static class Handler
    {
        /// <summary>
        /// Instance of the database.
        /// </summary>
        private static dbEntities carshopdatabase;

        /// <summary>
        /// Initializes static members of the <see cref="Handler"/> class.
        /// </summary>
        static Handler()
        {
            carshopdatabase = new dbEntities();
        }

        /// <summary>
        /// Gets data.
        /// </summary>
        public static dbEntities CarShopDataBase
        {
            get
            {
                return carshopdatabase;
            }
        }

        /// <summary>
        /// Gets data.
        /// </summary>
        public static List<string> TablesNames
        {
            get
            {
                return carshopdatabase.Database.SqlQuery<string>("SELECT name FROM sys.tables ORDER BY name").ToList();
            }
        }

        /// <summary>
        /// Gets brands.
        /// </summary>
        /// <returns>Return Parameter.</returns>
        public static List<brands> GetAllBrands()
        {
            var bquery = carshopdatabase.brands.Select(o => o);
            return bquery.ToList();
        }

        /// <summary>
        /// Gets extras.
        /// </summary>
        /// <returns>Return Parameter.</returns>
        public static List<extras> GetAllExtras()
        {
            var bquery = carshopdatabase.extras.Select(o => o);
            return bquery.ToList();
        }

        /// <summary>
        /// Gets models.
        /// </summary>
        /// <returns>Return Parameter.</returns>
        public static List<modells> GetAllModells()
        {
            var bquery = carshopdatabase.modells.Select(o => o);
            return bquery.ToList();
        }

        /// <summary>
        /// Gets model's extra's.
        /// </summary>
        /// <returns>Returns list.</returns>
        public static List<modellsextras> GetAllModellsExtras()
        {
            var bquery = carshopdatabase.modellsextras.Select(o => o);
            return bquery.ToList();
        }

        /// <summary>
        /// Gets last id from table.
        /// </summary>
        /// <param name="table">Table object</param>
        /// <returns>Last index.</returns>
        public static int GetLastId(object table)
        {
            int[] array = new int[100];
            if (table is brands)
            {
                array = carshopdatabase.brands.Select(x => x.Id).ToArray();
            }
            else if (table is extras)
            {
                array = carshopdatabase.extras.Select(x => x.Id).ToArray();
            }
            else if (table is modells)
            {
                array = carshopdatabase.modells.Select(x => x.Id).ToArray();
            }
            else
            {
                array = carshopdatabase.modellsextras.Select(x => x.Id).ToArray();
            }

            int re = array.Max() + 1;
            return re;
        }

        /// <summary>
        /// Saves data.
        /// </summary>
        public static void SaveDB()
        {
            carshopdatabase.SaveChanges();
        }

        /// <summary>
        /// Adds new record to database.
        /// </summary>
        /// <param name="brand">Table we work in.</param>
        public static void AddNewBrand(brands brand)
        {
            carshopdatabase.brands.Add(brand);
            SaveDB();
        }

        /// <summary>
        /// Adds new record to database.
        /// </summary>
        /// <param name="modell">Table we work in.</param>
        public static void AddNewModell(modells modell)
        {
            carshopdatabase.modells.Add(modell);
            SaveDB();
        }

        /// <summary>
        /// Adds new record to database.
        /// </summary>
        /// <param name="extra">Table we work in.</param>
        public static void AddNewExtra(extras extra)
        {
            carshopdatabase.extras.Add(extra);
            SaveDB();
        }

        /// <summary>
        /// Adds new record to database.
        /// </summary>
        /// <param name="me">Table we work in.</param>
        public static void AddNewModellExtra(modellsextras me)
        {
            carshopdatabase.modellsextras.Add(me);
            SaveDB();
        }

        /// <summary>
        /// Deletes a record.
        /// </summary>
        /// <param name="brand">Table of record.</param>
        public static void RemoveBrand(brands brand)
        {
            carshopdatabase.brands.Remove(brand);
            SaveDB();
        }

        /// <summary>
        /// Deletes a record.
        /// </summary>
        /// <param name="modell">Table of record.</param>
        public static void RemoveModell(modells modell)
        {
            carshopdatabase.modells.Remove(modell);
            SaveDB();
        }

        /// <summary>
        /// Deletes a record.
        /// </summary>
        /// <param name="extra">Table of record.</param>
        public static void RemoveExtra(extras extra)
        {
            carshopdatabase.extras.Remove(extra);
            SaveDB();
        }

        /// <summary>
        /// Deletes a record.
        /// </summary>
        /// <param name="me">Table of record.</param>
        public static void RemoveModellExtra(modellsextras me)
        {
            carshopdatabase.modellsextras.Remove(me);
            SaveDB();
        }
    }
}