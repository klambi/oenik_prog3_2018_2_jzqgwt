﻿//-----------------------------------------------------------------------
// <copyright file="ModellsExtrasTests.cs" company="Klambauer Richárd">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Logic;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for models repository.
    /// </summary>
    [TestFixture]
    public class ModellsExtrasTests
    {
        /// <summary>
        /// Check if method returns zero element.
        /// </summary>
        [Test]
        public void ModellsExtrasGetAllCountIsZero()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modellsextras> me = new List<modellsextras>();

            mockedL.Setup(x => x.Helper.ModellsExtrasRepo.GetAll()).Returns(me.AsQueryable());
            Assert.That(mockedL.Object.Helper.ModellsExtrasRepo.GetAll().Count(), Is.EqualTo(0));
            mockedL.Verify(mock => mock.Helper.ModellsExtrasRepo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Check if method returns more than zero element.
        /// </summary>
        [Test]
        public void ModellsExtrasGetAllCountIsNotZero()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modellsextras> me = new List<modellsextras>
            {
                new modellsextras() { Id = 1, extraid = 1, modellid = 1 }
            };

            mockedL.Setup(x => x.Helper.ModellsExtrasRepo.GetAll()).Returns(me.AsQueryable());
            Assert.That(mockedL.Object.Helper.ModellsExtrasRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Verify(mock => mock.Helper.ModellsExtrasRepo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test of creating new record. Just calling test.
        /// </summary>
        [Test]
        public void CreateModellsExtrasCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modellsextras> me = new List<modellsextras>
            {
                new modellsextras() { Id = 1, extraid = 1, modellid = 1 }
            };

            mockedL.Setup(x => x.Helper.ModellsExtrasRepo.GetAll()).Returns(me.AsQueryable());
            Assert.That(mockedL.Object.Helper.ModellsExtrasRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Object.Helper.ModellsExtrasRepo.Create(1, 1);
            mockedL.Verify(mock => mock.Helper.ModellsExtrasRepo.Create(1, 1), Times.Once);
        }

        /// <summary>
        /// Test of searching for id. Just calling test.
        /// </summary>
        [Test]
        public void ModellsExtrasExistCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modellsextras> me = new List<modellsextras>();
            int i = 1;
            while (i < 47)
            {
                me.Add(new modellsextras() { Id = i, extraid = i, modellid = i });
                i++;
            }

            mockedL.Setup(x => x.Helper.ModellsExtrasRepo.GetAll()).Returns(me.AsQueryable());
            Assert.That(mockedL.Object.Helper.ModellsExtrasRepo.GetAll().Count(), Is.EqualTo(46));
            mockedL.Object.Helper.ModellsExtrasRepo.CheckIfModellsExtrasIdExists(1);
            mockedL.Verify(mock => mock.Helper.ModellsExtrasRepo.CheckIfModellsExtrasIdExists(1), Times.Once);
        }

        /// <summary>
        /// Test of deleting a record. Just calling test.
        /// </summary>
        [Test]
        public void DeleteModellsExtrasCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modellsextras> me = new List<modellsextras>();

            me.Add(new modellsextras() { Id = 1, extraid = 1, modellid = 1 });

            mockedL.Setup(x => x.Helper.ModellsExtrasRepo.GetAll()).Returns(me.AsQueryable());
            Assert.That(mockedL.Object.Helper.ModellsExtrasRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Object.Helper.ModellsExtrasRepo.Remove(1);
            mockedL.Verify(mock => mock.Helper.ModellsExtrasRepo.Remove(1), Times.Once);
        }

        /// <summary>
        /// Test of getting records. Just calling test.
        /// </summary>
        [Test]
        public void GetAllModellsExtrasCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modellsextras> me = new List<modellsextras>();
            int i = 1;
            while (i < 47)
            {
                me.Add(new modellsextras() { Id = i, extraid = 1, modellid = 1 });
                i++;
            }

            mockedL.Setup(x => x.Helper.ModellsExtrasRepo.GetAll()).Returns(me.AsQueryable());

            Assert.That(mockedL.Object.Helper.ModellsExtrasRepo.GetAll().Count(), Is.EqualTo(46));
            mockedL.Verify(mock => mock.Helper.ModellsExtrasRepo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test of getting table content. Just calling test.
        /// </summary>
        [Test]
        public void GetModellsExtrasTableContentCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modellsextras> me = new List<modellsextras>();
            me.Add(new modellsextras() { Id = 1, modellid = 1, extraid = 1 });

            mockedL.Setup(x => x.Helper.ModellsExtrasRepo.GetAll()).Returns(me.AsQueryable());
            Assert.That(mockedL.Object.Helper.ModellsExtrasRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Object.Helper.ModellsExtrasRepo.GetTableContents();
            mockedL.Verify(mock => mock.Helper.ModellsExtrasRepo.GetTableContents(), Times.Once);
        }
    }
}
