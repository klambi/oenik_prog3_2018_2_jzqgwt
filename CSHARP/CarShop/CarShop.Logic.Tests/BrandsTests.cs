﻿//-----------------------------------------------------------------------
// <copyright file="BrandsTests.cs" company="Klambauer Richárd">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Logic;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for models repository.
    /// </summary>
    [TestFixture]
    public class BrandsTests
    {
        /// <summary>
        /// Check if method returns zero element.
        /// </summary>
        [Test]
        public void BrandGetAllCountIsZero()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<brands> b = new List<brands>();

            mockedL.Setup(x => x.Helper.BrandsRepo.GetAll()).Returns(b.AsQueryable());
            Assert.That(mockedL.Object.Helper.BrandsRepo.GetAll().Count(), Is.EqualTo(0));
            mockedL.Verify(mock => mock.Helper.BrandsRepo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Check if method returns more than zero element.
        /// </summary>
        [Test]
        public void BrandGetAllCountIsNotZero()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<brands> b = new List<brands>
            {
                new brands() { Id = 1, name = string.Empty, country = string.Empty, url = string.Empty, foundation = 0, income = 0 }
            };

            mockedL.Setup(x => x.Helper.BrandsRepo.GetAll()).Returns(b.AsQueryable());
            Assert.That(mockedL.Object.Helper.BrandsRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Verify(mock => mock.Helper.BrandsRepo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test of creating new record. Just calling test.
        /// </summary>
        [Test]
        public void CreateBrandCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<brands> b = new List<brands>
            {
                new brands() { Id = 1, name = string.Empty, country = string.Empty, url = string.Empty, foundation = 0, income = 0 }
            };

            mockedL.Setup(x => x.Helper.BrandsRepo.GetAll()).Returns(b.AsQueryable());
            Assert.That(mockedL.Object.Helper.BrandsRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Object.Helper.BrandsRepo.Create(string.Empty, string.Empty, string.Empty, 0, 0);
            mockedL.Verify(mock => mock.Helper.BrandsRepo.Create(string.Empty, string.Empty, string.Empty, 0, 0), Times.Once);
        }

        /// <summary>
        /// Test of searching for id. Just calling test.
        /// </summary>
        [Test]
        public void BrandExistCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<brands> b = new List<brands>();
            int i = 1;
            while (i < 47)
            {
                b.Add(new brands() { Id = i, name = string.Empty, country = string.Empty, url = string.Empty + i, foundation = 0, income = 0 });
                i++;
            }

            mockedL.Setup(x => x.Helper.BrandsRepo.GetAll()).Returns(b.AsQueryable());
            Assert.That(mockedL.Object.Helper.BrandsRepo.GetAll().Count(), Is.EqualTo(46));
            mockedL.Object.Helper.BrandsRepo.CheckIfBrandExists(1);
            mockedL.Verify(mock => mock.Helper.BrandsRepo.CheckIfBrandExists(1), Times.Once);
        }

        /// <summary>
        /// Test of deleting a record. Just calling test.
        /// </summary>
        [Test]
        public void DeleteBrandCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<brands> b = new List<brands>();
            b.Add(new brands() { Id = 1, name = string.Empty, country = string.Empty, url = string.Empty, foundation = 0, income = 0 });

            mockedL.Setup(x => x.Helper.BrandsRepo.GetAll()).Returns(b.AsQueryable());
            Assert.That(mockedL.Object.Helper.BrandsRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Object.Helper.BrandsRepo.Remove(1);
            mockedL.Verify(mock => mock.Helper.BrandsRepo.Remove(1), Times.Once);
        }

        /// <summary>
        /// Test of updating a record. Just calling test.
        /// </summary>
        [Test]
        public void UpdateBrandCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<brands> b = new List<brands>
            {
                new brands() { Id = 1, name = string.Empty, country = string.Empty, url = string.Empty + 1, foundation = 0, income = 0 }
            };

            mockedL.Setup(x => x.Helper.BrandsRepo.GetAll()).Returns(b.AsQueryable());
            Assert.That(mockedL.Object.Helper.BrandsRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Object.Helper.BrandsRepo.Update(1, string.Empty, string.Empty, string.Empty, 0, 0);
            mockedL.Verify(mock => mock.Helper.BrandsRepo.Update(1, string.Empty, string.Empty, string.Empty, 0, 0), Times.Once);
        }

        /// <summary>
        /// Test of getting records. Just calling test.
        /// </summary>
        [Test]
        public void GetAllBrandCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<brands> b = new List<brands>();
            int i = 1;
            while (i < 47)
            {
                b.Add(new brands() { Id = i, name = string.Empty, country = string.Empty, url = string.Empty + i, foundation = 0, income = 0 });
                i++;
            }

            mockedL.Setup(x => x.Helper.BrandsRepo.GetAll()).Returns(b.AsQueryable());

            Assert.That(mockedL.Object.Helper.BrandsRepo.GetAll().Count(), Is.EqualTo(46));
            mockedL.Verify(mock => mock.Helper.BrandsRepo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test of getting table content. Just calling test.
        /// </summary>
        [Test]
        public void GetBrandTableContentCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<brands> b = new List<brands>();
            int i = 1;
            while (i < 47)
            {
                b.Add(new brands() { Id = i, name = string.Empty, country = string.Empty, url = string.Empty + i, foundation = 0, income = 0 });
                i++;
            }

            mockedL.Setup(x => x.Helper.BrandsRepo.GetAll()).Returns(b.AsQueryable());
            Assert.That(mockedL.Object.Helper.BrandsRepo.GetAll().Count(), Is.EqualTo(46));
            mockedL.Object.Helper.BrandsRepo.GetTableContents();
            mockedL.Verify(mock => mock.Helper.BrandsRepo.GetTableContents(), Times.Once);
        }
    }
}
