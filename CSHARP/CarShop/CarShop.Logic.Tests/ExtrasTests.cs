﻿//-----------------------------------------------------------------------
// <copyright file="ExtrasTests.cs" company="Klambauer Richárd">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Logic;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for models repository.
    /// </summary>
    [TestFixture]
    public class ExtrasTests
    {
        /// <summary>
        /// Test of data received from method is null or zero.
        /// </summary>
        [Test]
        public void ExtraGetAllCountIsZero()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<extras> e = new List<extras>();

            mockedL.Setup(x => x.Helper.ExtrasRepo.GetAll()).Returns(e.AsQueryable());
            Assert.That(mockedL.Object.Helper.ExtrasRepo.GetAll().Count(), Is.EqualTo(0));
            mockedL.Verify(mock => mock.Helper.ExtrasRepo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test of data received from method is more than zero.
        /// </summary>
        [Test]
        public void ExtraGetAllCountIsNotZero()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<extras> e = new List<extras>();

            e.Add(new extras() { Id = 1, category = string.Empty, color = string.Empty, name = string.Empty, price = 0, reusable = 1 });

            mockedL.Setup(x => x.Helper.ExtrasRepo.GetAll()).Returns(e.AsQueryable());
            Assert.That(mockedL.Object.Helper.ExtrasRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Verify(mock => mock.Helper.ExtrasRepo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test of creating new record. Just calling test.
        /// </summary>
        [Test]
        public void CreateExtraCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<extras> e = new List<extras>();

            e.Add(new extras() { Id = 1, category = string.Empty, color = string.Empty, name = string.Empty, price = 0, reusable = 1 });

            mockedL.Setup(x => x.Helper.ExtrasRepo.GetAll()).Returns(e.AsQueryable());
            Assert.That(mockedL.Object.Helper.ExtrasRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Object.Helper.ExtrasRepo.Create(string.Empty, string.Empty, 0, string.Empty, true);
            mockedL.Verify(mock => mock.Helper.ExtrasRepo.Create(string.Empty, string.Empty, 0, string.Empty, true), Times.Once);
        }

        /// <summary>
        /// Test of searching for id. Just calling test.
        /// </summary>
        [Test]
        public void ExtraExistCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<extras> e = new List<extras>();
            e.Add(new extras() { Id = 1, category = string.Empty, color = string.Empty, name = string.Empty, price = 0, reusable = 1 });

            mockedL.Setup(x => x.Helper.ExtrasRepo.GetAll()).Returns(e.AsQueryable());
            Assert.That(mockedL.Object.Helper.ExtrasRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Object.Helper.ExtrasRepo.CheckIfExtraIdExist(1);
            mockedL.Verify(mock => mock.Helper.ExtrasRepo.CheckIfExtraIdExist(1), Times.Once);
        }

        /// <summary>
        /// Test of deleting a record. Just calling test.
        /// </summary>
        [Test]
        public void DeleteExtraCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<extras> e = new List<extras>();
            e.Add(new extras() { Id = 1, category = string.Empty, color = string.Empty, name = string.Empty, price = 0, reusable = 1 });
            e.Add(new extras() { Id = 2, category = string.Empty, color = string.Empty, name = string.Empty, price = 0, reusable = 1 });

            mockedL.Setup(x => x.Helper.ExtrasRepo.GetAll()).Returns(e.AsQueryable());
            Assert.That(mockedL.Object.Helper.ExtrasRepo.GetAll().Count(), Is.EqualTo(2));
            mockedL.Object.Helper.ExtrasRepo.Remove(1);
            mockedL.Verify(mock => mock.Helper.ExtrasRepo.Remove(1), Times.Once);
        }

        /// <summary>
        /// Test of updating a record. Just calling test.
        /// </summary>
        [Test]
        public void UpdateExtraCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<extras> e = new List<extras>();

            e.Add(new extras() { Id = 1, category = string.Empty, color = string.Empty, name = string.Empty, price = 0, reusable = 1 });

            mockedL.Setup(x => x.Helper.ExtrasRepo.GetAll()).Returns(e.AsQueryable());
            Assert.That(mockedL.Object.Helper.ExtrasRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Object.Helper.ExtrasRepo.Update(1, string.Empty, string.Empty, 0, string.Empty, true);
            mockedL.Verify(mock => mock.Helper.ExtrasRepo.Update(1, string.Empty, string.Empty, 0, string.Empty, true), Times.Once);
        }

        /// <summary>
        /// Test of getting records. Just calling test.
        /// </summary>
        [Test]
        public void GetAllExtraCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<extras> e = new List<extras>();

            int i = 1;
            while (i < 47)
            {
                e.Add(new extras() { Id = i, category = string.Empty, color = string.Empty, name = string.Empty, price = 0, reusable = 1 });
                i++;
            }

            mockedL.Setup(x => x.Helper.ExtrasRepo.GetAll()).Returns(e.AsQueryable());

            Assert.That(mockedL.Object.Helper.ExtrasRepo.GetAll().Count(), Is.EqualTo(46));
            mockedL.Verify(mock => mock.Helper.ExtrasRepo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test of getting table content. Just calling test.
        /// </summary>
        [Test]
        public void GetExtraTableContentCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<extras> e = new List<extras>();
            e.Add(new extras() { Id = 1, category = string.Empty, color = string.Empty, name = string.Empty, price = 0, reusable = 1 });

            mockedL.Setup(x => x.Helper.ExtrasRepo.GetAll()).Returns(e.AsQueryable());
            Assert.That(mockedL.Object.Helper.ExtrasRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Object.Helper.ExtrasRepo.GetTableContents();
            mockedL.Verify(mock => mock.Helper.ExtrasRepo.GetTableContents(), Times.Once);
        }
    }
}
