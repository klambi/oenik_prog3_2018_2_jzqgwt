﻿//-----------------------------------------------------------------------
// <copyright file="ModellsTests.cs" company="Klambauer Richárd">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace CarShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;
    using CarShop.Logic;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for models repository.
    /// </summary>
    [TestFixture]
    public class ModellsTests
    {
        /// <summary>
        /// Check if method returns zero element.
        /// </summary>
        [Test]
        public void ModellsExtrasGetAllCountIsZero()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modells> m = new List<modells>();

            mockedL.Setup(x => x.Helper.ModellsRepo.GetAll()).Returns(m.AsQueryable());
            Assert.That(mockedL.Object.Helper.ModellsRepo.GetAll().Count(), Is.EqualTo(0));
            mockedL.Verify(mock => mock.Helper.ModellsRepo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Check if method returns more than zero element.
        /// </summary>
        [Test]
        public void ModellsExtrasGetAllCountIsNotZero()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modells> m = new List<modells>
            {
                new modells() { Id = 1, price = 0, brand_id = 1, date = default(DateTime), engineinfo = 0, hp = 0, name = string.Empty }
            };

            mockedL.Setup(x => x.Helper.ModellsRepo.GetAll()).Returns(m.AsQueryable());
            Assert.That(mockedL.Object.Helper.ModellsRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Verify(mock => mock.Helper.ModellsRepo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test of creating new record. Just calling test.
        /// </summary>
        [Test]
        public void CreateModellCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modells> m = new List<modells>
            {
                new modells() { Id = 1, price = 0, brand_id = 1, date = default(DateTime), engineinfo = 0, hp = 0, name = string.Empty }
            };

            mockedL.Setup(x => x.Helper.ModellsRepo.GetAll()).Returns(m.AsQueryable());

            mockedL.Object.Helper.ModellsRepo.Create(1, string.Empty, default(DateTime), 0, 0, 0);
            Assert.That(mockedL.Object.Helper.ModellsRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Verify(mock => mock.Helper.ModellsRepo.Create(1, string.Empty, default(DateTime), 0, 0, 0), Times.Once);
        }

        /// <summary>
        /// Test of searching for id. Just calling test.
        /// </summary>
        [Test]
        public void ModellExistCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modells> m = new List<modells>
            {
                new modells() { Id = 1, price = 0, brand_id = 1, date = default(DateTime), engineinfo = 0, hp = 0, name = string.Empty }
            };

            mockedL.Setup(x => x.Helper.ModellsRepo.GetAll()).Returns(m.AsQueryable());
            Assert.That(mockedL.Object.Helper.ModellsRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Object.Helper.ModellsRepo.CheckIfModellIdExist(1);
            mockedL.Verify(mock => mock.Helper.ModellsRepo.CheckIfModellIdExist(1), Times.Once);
        }

        /// <summary>
        /// Test of deleting a record. Just calling test.
        /// </summary>
        [Test]
        public void DeleteModellCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modells> m = new List<modells>
            {
                new modells() { Id = 1, price = 0, brand_id = 1, date = default(DateTime), engineinfo = 0, hp = 0, name = string.Empty }
            };

            mockedL.Setup(x => x.Helper.ModellsRepo.GetAll()).Returns(m.AsQueryable());
            Assert.That(mockedL.Object.Helper.ModellsRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Object.Helper.ModellsRepo.Remove(1);
            mockedL.Verify(mock => mock.Helper.ModellsRepo.Remove(1), Times.Once);
        }

        /// <summary>
        /// Test of updating a record. Just calling test.
        /// </summary>
        [Test]
        public void UpdateModellCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modells> m = new List<modells>
            {
                new modells() { Id = 1, price = 0, brand_id = 1, date = default(DateTime), engineinfo = 0, hp = 0, name = string.Empty }
            };

            mockedL.Setup(x => x.Helper.ModellsRepo.GetAll()).Returns(m.AsQueryable());
            Assert.That(mockedL.Object.Helper.ModellsRepo.GetAll().Count(), Is.EqualTo(1));
            mockedL.Object.Helper.ModellsRepo.Update(1, 2, string.Empty, default(DateTime), 0, 0, 0);
            mockedL.Verify(mock => mock.Helper.ModellsRepo.Update(1, 2, string.Empty, default(DateTime), 0, 0, 0), Times.Once);
        }

        /// <summary>
        /// Test of getting records. Just calling test.
        /// </summary>
        [Test]
        public void GetAllModellCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modells> m = new List<modells>();
            int i = 1;
            while (i < 47)
            {
                m.Add(new modells() { Id = i, price = 0, brand_id = 1, date = default(DateTime), engineinfo = 0, hp = 0, name = string.Empty });
                i++;
            }

            mockedL.Setup(x => x.Helper.ModellsRepo.GetAll()).Returns(m.AsQueryable());

            Assert.That(mockedL.Object.Helper.ModellsRepo.GetAll().Count(), Is.EqualTo(46));
            mockedL.Verify(mock => mock.Helper.ModellsRepo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test of getting table content. Just calling test.
        /// </summary>
        [Test]
        public void GetModellTableContentCallCheck()
        {
            Logic l = Mock.Of<Logic>();
            Mock<Logic> mockedL = Mock.Get(l);
            List<modells> m = new List<modells>();
            int i = 1;
            while (i < 47)
            {
                m.Add(new modells() { Id = i, price = 0, brand_id = 1, date = default(DateTime), engineinfo = 0, hp = 0, name = string.Empty });
                i++;
            }

            mockedL.Setup(x => x.Helper.ModellsRepo.GetAll()).Returns(m.AsQueryable());
            Assert.That(mockedL.Object.Helper.ModellsRepo.GetAll().Count(), Is.EqualTo(46));
            mockedL.Object.Helper.ModellsRepo.GetTableContents();
            mockedL.Verify(mock => mock.Helper.ModellsRepo.GetTableContents(), Times.Once);
        }
    }
}
